package com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

public class CheckOutInteractor {

    //High priority variables goes below.....
    private CheckOutResponseListener checkOutResponseListener;
    private TRAVEL_TYPE_ENUM travelType;

    //Medium Priority Variable
    private Context mContext;
    private Integer mPos;


    //Least priority variables goes below.....
    private final String TAG = "CheckOutInteractor";
    private final String employeeCheckOutURL = "EmployeeCheckOut";//http://124.30.44.228/ETMSMOBILE.Api/api/EmployeeCheckOut


    public CheckOutInteractor(CheckOutResponseListener checkOutListener, TRAVEL_TYPE_ENUM travelType, Context context) {
        this.checkOutResponseListener = checkOutListener;
        this.travelType = travelType;
        mContext = context;
    }//constructor closes here....


    public void execute(final Map<String, String> params, Integer pos) {
        mPos = pos;
        if (this.checkOutResponseListener == null)
            throw new NullPointerException("pickupDetailsListener cannot be null.");
        else {
            String apiUrl = BuildConfig.SERVER_URL + employeeCheckOutURL;
            Log.d(TAG, "URL = " + apiUrl);
            Log.d(TAG, "Params = " + params);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, apiUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "Response is : " + response);

//                            //TODO: Remove BELOW LINE WHEN MERGING TO MASTER....
//                            response = "{\n" +
//                                    "    \"status\": true,\n" +
//                                    "    \"message\": \"Checked out successfully.\",\n" +
//                                    "    \"data\": null,\n" +
//                                    "    \"totalRecords\": 0\n" +
//                                    "}";

                            CheckOutResponseModel checkOutResponseModel = new Gson().fromJson(response, CheckOutResponseModel.class);

                            ////////////...........PARSING RESPONSE..........\\\\\\\\\\\\\\
                            if (checkOutResponseModel.status) {
                                //Status is true.
                                checkOutResponseListener.employeeCheckOutSuccessFully(checkOutResponseModel, travelType, mPos);
                            }//if(startTripResponseModel.status) closes here....
                            else {
                                //Status is false.
                                checkOutResponseListener.employeeCheckOutFailure(checkOutResponseModel.message);
                            }//else closes here....
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getActivity(), "ErrorListener \n " + error.toString(), Toast.LENGTH_LONG).show();
                            checkOutResponseListener.employeeCheckOutFailure(error.getMessage() + " " + error.toString());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");//TODO: Token is Hardcoded, need to softcode it.
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    return params;
                }

            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                    AppConstants.VOLLEY_MAX_RETRIES,
                    AppConstants.VOLLEY_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this.mContext);
            requestQueue.add(stringRequest);
        }//else listener is not null closes here......
    }

}//StartTripInteractor closes here.....
