package com.hgs.etms.Dashboard.Employee.GetAllTripsListing;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

import androidx.annotation.Nullable;

public class GetAllTripsDataModel implements Parcelable {

    public int recordid;
    public String empid;
    public String scheduled_date;
    public String gender;
    public String slot;
    public String slot_type;
    public String home_location;
    public String created_date;
    public String created_by;
    public String delete_flag;
    public String vehicle_id;
    public String driver_name;
    public String driver_No;
    @Nullable public boolean is_checked_in;
    @Nullable public boolean is_checked_out;
    @Nullable public boolean is_feedback_submited;


    protected GetAllTripsDataModel(Parcel in) {
        recordid = in.readInt();
        empid = in.readString();
        scheduled_date = in.readString();
        gender = in.readString();
        slot = in.readString();
        slot_type = in.readString();
        home_location = in.readString();
        created_date = in.readString();
        created_by = in.readString();
        delete_flag = in.readString();
        vehicle_id = in.readString();
        driver_name = in.readString();
        driver_No = in.readString();
        is_checked_in = in.readByte() != 0x00;
        is_checked_out = in.readByte() != 0x00;
        is_feedback_submited = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(recordid);
        dest.writeString(empid);
        dest.writeString(scheduled_date);
        dest.writeString(gender);
        dest.writeString(slot);
        dest.writeString(slot_type);
        dest.writeString(home_location);
        dest.writeString(created_date);
        dest.writeString(created_by);
        dest.writeString(delete_flag);
        dest.writeString(vehicle_id);
        dest.writeString(driver_name);
        dest.writeString(driver_No);
        dest.writeByte((byte) (is_checked_in ? 0x01 : 0x00));
        dest.writeByte((byte) (is_checked_out ? 0x01 : 0x00));
        dest.writeByte((byte) (is_feedback_submited ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GetAllTripsDataModel> CREATOR = new Parcelable.Creator<GetAllTripsDataModel>() {
        @Override
        public GetAllTripsDataModel createFromParcel(Parcel in) {
            return new GetAllTripsDataModel(in);
        }

        @Override
        public GetAllTripsDataModel[] newArray(int size) {
            return new GetAllTripsDataModel[size];
        }
    };






    public static Comparator<GetAllTripsDataModel> EMPLOYEE_PICKUP_DROP_SLOT_COMPARATOR
            = new Comparator<GetAllTripsDataModel>() {

        public int compare(GetAllTripsDataModel obj1, GetAllTripsDataModel obj2) {

            String fruitName1 = obj1.slot_type.toUpperCase();
            String fruitName2 = obj2.slot_type.toUpperCase();

            //ascending order
            return fruitName1.compareTo(fruitName2);

            //descending order
//            return fruitName2.compareTo(fruitName1);
        }

    };
}//GetAllTripsDataModel closes here.....
