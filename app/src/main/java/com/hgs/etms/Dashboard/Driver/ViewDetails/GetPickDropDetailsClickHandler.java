package com.hgs.etms.Dashboard.Driver.ViewDetails;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;

import com.hgs.etms.Dashboard.Driver.PickupDropDataModel;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.GetTravelDetails.GetTravelDetailsInteractor;
import com.hgs.etms.GetTravelDetails.GetTravelDetailsListener;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.GetTravelDetails.TravelDetailsModel;
import com.hgs.etms.utils.AppConstants;

import androidx.preference.PreferenceManager;

public class GetPickDropDetailsClickHandler implements View.OnClickListener, GetTravelDetailsListener {


    //High priority variables goes below.....
    private final GetPickupDropDetailsListener mPickUpDropViewDetailsListener;
    private final Object mPickupDropModel;
    private final int clickedPostion;
    private final Context mContext;
    private final TRAVEL_TYPE_ENUM mTravelType;
    private ProgressDialog progressDialog;


    public GetPickDropDetailsClickHandler(Context context, GetPickupDropDetailsListener mListener, PickupDropDataModel pickupDropDataModel, int position, TRAVEL_TYPE_ENUM traveltype) {
        this.mContext = context;
        this.mPickUpDropViewDetailsListener = mListener;
        this.mPickupDropModel = pickupDropDataModel;
        this.clickedPostion = position;
        this.mTravelType = traveltype;
    }//GetPickDropDetailsClickHandler constructor closes here....



    public GetPickDropDetailsClickHandler(Context context, GetPickupDropDetailsListener mListener, GetAllTripsDataModel pickupDropDataModel, int position, TRAVEL_TYPE_ENUM traveltype) {
        this.mContext = context;
        this.mPickUpDropViewDetailsListener = mListener;
        this.mPickupDropModel = pickupDropDataModel;
        this.clickedPostion = position;
        this.mTravelType = traveltype;
    }//GetPickDropDetailsClickHandler constructor closes here....


    @Override
    public void onClick(View view) {

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Fetching Details....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        //Creating params to fetch the details.....
        String scheduledDate = null;
        String scheduledSlot = null;
        String driverName = null;
        if(mPickupDropModel instanceof PickupDropDataModel) {
            PickupDropDataModel model = (PickupDropDataModel) mPickupDropModel;
            scheduledDate = model.scheduled_date;
            scheduledSlot = model.slot;
            driverName = PreferenceManager.getDefaultSharedPreferences(mContext).getString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, "");
        }//if(mPickupDropModel instanceof PickupDropDataModel) closes here.....
        else if(mPickupDropModel instanceof  GetAllTripsDataModel){
            GetAllTripsDataModel model = (GetAllTripsDataModel) mPickupDropModel;
            scheduledDate = model.scheduled_date;
            scheduledSlot = model.slot;
            driverName = model.driver_name;
        }//else if(mPickupDropModel instanceof  GetAllTripsDataModel) closes here....




        //We will call API here & fetch the details.....
        GetTravelDetailsInteractor getTravelDetails = new GetTravelDetailsInteractor(this, mContext, mTravelType);
        getTravelDetails.getTravelDetails(scheduledDate, scheduledSlot, driverName);
    }//onClick closes here.....

    @Override
    public void travelDetailsSuccess(TravelDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType) {



        if(travelDetailsModel.data != null && !travelDetailsModel.data.isEmpty()){//Not condition....
            mPickUpDropViewDetailsListener.pickupDropdetailsFetchedSuccessfully(travelDetailsModel, clickedPostion, travelType);
        }//if(travelDetailsModel.data != null && !travelDetailsModel.data.isEmpty()) closes here.....
        else{
            mPickUpDropViewDetailsListener.pickupDropDetailsFetchedFailure("Oops, no passengers available for this trip.");
        }//else closes here.....

       if(progressDialog!=null)
        progressDialog.dismiss();
    }//travelDetailsSuccess closes here.....

    @Override
    public void travelDetailsFailure(String errorMessage) {
        mPickUpDropViewDetailsListener.pickupDropDetailsFetchedFailure(errorMessage);
        if(progressDialog!=null)
            progressDialog.dismiss();
    }//travelDetailsFailure closes here.....

}//GetPickDropDetailsClickHandler class closes here.....
