package com.hgs.etms.Dashboard.Driver;

import java.io.Serializable;

public class PickupDropSlotModel implements Serializable {

    public String scheduleDate;
    public String slot;
    public String slot_type;

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getSlot_type() {
        return slot_type;
    }

    public void setSlot_type(String slot_type) {
        this.slot_type = slot_type;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    @Override
    public String toString() {
        return "PickupDropSlotModel{" +
                "scheduleDate='" + scheduleDate + '\'' +
                ", slot='" + slot + '\'' +
                ", slot_type='" + slot_type + '\'' +
                '}';
    }
}