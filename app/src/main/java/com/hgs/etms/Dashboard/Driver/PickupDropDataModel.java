package com.hgs.etms.Dashboard.Driver;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.Comparator;

public class PickupDropDataModel implements Parcelable{

    public String scheduled_date;
    public String slot;
    public String slot_type;
    public String driver_name;
    public String driver_No;
    public String vehicle_id;
    public int passengerCount;
    public Boolean isTripStarted;
    public Boolean isTripStoped;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected PickupDropDataModel(Parcel in) {
        scheduled_date = in.readString();
        slot = in.readString();
        slot_type = in.readString();
        driver_name = in.readString();
        driver_No = in.readString();
        vehicle_id = in.readString();
        passengerCount = in.readInt();
        isTripStarted = in.readBoolean();
        isTripStoped = in.readBoolean();
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(scheduled_date);
        dest.writeString(slot);
        dest.writeString(slot_type);
        dest.writeString(driver_name);
        dest.writeString(driver_No);
        dest.writeString(vehicle_id);
        dest.writeInt(passengerCount);
        dest.writeBoolean(isTripStarted);
        dest.writeBoolean(isTripStoped);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PickupDropDataModel> CREATOR = new Parcelable.Creator<PickupDropDataModel>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public PickupDropDataModel createFromParcel(Parcel in) {
            return new PickupDropDataModel(in);
        }

        @Override
        public PickupDropDataModel[] newArray(int size) {
            return new PickupDropDataModel[size];
        }
    };

//
//    @Override
//    public int compareTo(PickupDropDataModel pickupDropDataModel) {
//        return 0;
//    }//compareTo closes here....



    public static Comparator<PickupDropDataModel> FruitNameComparator
            = new Comparator<PickupDropDataModel>() {

        public int compare(PickupDropDataModel obj1, PickupDropDataModel obj2) {

            String fruitName1 = obj1.slot_type.toUpperCase();
            String fruitName2 = obj2.slot_type.toUpperCase();

            //ascending order
            return fruitName1.compareTo(fruitName2);

            //descending order
//            return fruitName2.compareTo(fruitName1);
        }

    };
}//PickupDropDataModel closes here....
