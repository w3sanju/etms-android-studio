package com.hgs.etms.Dashboard.Driver.StopTripHandler;

import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;

public interface StopTripResponseListener {

    void driverStoppedTripSuccessfully(StopTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType,Integer pos);

    void driverStoppedTripFailure(String errorMessage);
}//StopTripResponseListener closes here....
