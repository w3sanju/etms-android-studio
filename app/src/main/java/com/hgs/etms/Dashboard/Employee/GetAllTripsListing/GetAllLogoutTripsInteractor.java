package com.hgs.etms.Dashboard.Employee.GetAllTripsListing;


import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

import androidx.preference.PreferenceManager;

/**
 * Since as per the API Developer,
 * they send all dates in 1 response we have to filter the data manually.
 * There we call this API & save the data.
 * **/
public class GetAllLogoutTripsInteractor {

    //High priority variables goes below....
    private GetAllLogoutTripsListener mReponseListener;
    private Context mContext;


    //Least priority variables goes below...
    private final String TAG = "GetAllLogoutTripsInteractor".toString().substring(0,23);


    public GetAllLogoutTripsInteractor(GetAllLogoutTripsListener mReponseListener, Context context) {
        this.mReponseListener = mReponseListener;
        this.mContext = context;
    }


    public void execute(String leastPickupDate){
        if(this.mReponseListener == null)
            throw new NullPointerException("responseListener cannot be null.");
        else {
            final String employeeID = PreferenceManager.getDefaultSharedPreferences(mContext).getString(AppConstants.SF_LOGGED_IN_USER_EMP_ID, "");
            final String scheduleDate = leastPickupDate;

            Log.d(TAG, "URL is : "+BuildConfig.SERVER_URL + "EmployeeDrop");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.SERVER_URL + "EmployeeDrop",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d(TAG, "Response is : " + response);

                            ////////////...........PARSING RESPONSE..........\\\\\\\\\\\\\\
                            GetAllTripsResponseModel employeePickupDetails = new Gson().fromJson(response, GetAllTripsResponseModel.class);
                            if(employeePickupDetails.status){//if status is true....
                                if(!employeePickupDetails.data.isEmpty()){//Not condition....
                                    mReponseListener.allLogoutEmployeeTripFetchedSuccessfully(employeePickupDetails, employeePickupDetails.message);
                                }//if(!employeePickupDetails.data.isEmpty()) closes here....
                                else
                                    mReponseListener.allLogoutEmployeeTripFetchFailure(employeePickupDetails.message);
                            }//if(employeePickupDetails.status) closes here....
                            else{
                                mReponseListener.allLogoutEmployeeTripFetchFailure(employeePickupDetails.message);
                            }//else closes here....
                        }//onResponse closes here....

                    },//new Response.Listener<String>() closes here.....
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mReponseListener.allLogoutEmployeeTripFetchFailure(error.getMessage());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("employeeId", employeeID);
                    params.put("scheduledDate", scheduleDate);

                    Log.d(TAG, "Params for Employee Details : "+params);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                    AppConstants.VOLLEY_MAX_RETRIES,
                    AppConstants.VOLLEY_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            requestQueue.add(stringRequest);
        }//else listener is not null closes here......
    }//execute closes here....
}//GetAllLogoutTripsInteractor closes here.....
