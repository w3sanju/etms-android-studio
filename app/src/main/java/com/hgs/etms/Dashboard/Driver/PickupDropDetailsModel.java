package com.hgs.etms.Dashboard.Driver;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class PickupDropDetailsModel implements Parcelable {

    public boolean status;
    public String message;
    @Nullable public List<PickupDropDataModel> data;
    public int totalRecords;

    protected PickupDropDetailsModel(Parcel in) {
        status = in.readByte() != 0x00;
        message = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<PickupDropDataModel>();
            in.readList(data, PickupDropDataModel.class.getClassLoader());
        } else {
            data = null;
        }
        totalRecords = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (status ? 0x01 : 0x00));
        dest.writeString(message);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        dest.writeInt(totalRecords);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PickupDropDetailsModel> CREATOR = new Parcelable.Creator<PickupDropDetailsModel>() {
        @Override
        public PickupDropDetailsModel createFromParcel(Parcel in) {
            return new PickupDropDetailsModel(in);
        }

        @Override
        public PickupDropDetailsModel[] newArray(int size) {
            return new PickupDropDetailsModel[size];
        }
    };


}//PickupDropDetailsModel closes here....
