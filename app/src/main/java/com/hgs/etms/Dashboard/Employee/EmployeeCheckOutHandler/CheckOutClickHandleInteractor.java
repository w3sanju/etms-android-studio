package com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler;

@Deprecated
public interface CheckOutClickHandleInteractor {

    void checkedOutSuccessfully(String responseMessage, CheckOutResponseModel checkOutResponseModel, int position);

    void checkOutFailure(String errorMessage);
}
