package com.hgs.etms.Dashboard.Employee.DashboardViewPager;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomPagerModel implements Parcelable {

    public String date;//DD_MM_YYYY_DATEFORMAT in this format only....
    public boolean isToday;//Whether today is this date or not....


    public CustomPagerModel(Parcel in) {
        date = in.readString();
        isToday = in.readByte() != 0x00;
    }

    public CustomPagerModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeByte((byte) (isToday ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CustomPagerModel> CREATOR = new Parcelable.Creator<CustomPagerModel>() {
        @Override
        public CustomPagerModel createFromParcel(Parcel in) {
            return new CustomPagerModel(in);
        }

        @Override
        public CustomPagerModel[] newArray(int size) {
            return new CustomPagerModel[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isToday() {
        return isToday;
    }

    public void setToday(boolean today) {
        isToday = today;
    }
}//CustomPagerModel closes here....
