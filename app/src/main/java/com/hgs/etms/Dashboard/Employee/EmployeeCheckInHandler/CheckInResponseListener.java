package com.hgs.etms.Dashboard.Employee.EmployeeCheckInHandler;

import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;

public interface CheckInResponseListener {

    void employeeCheckInSuccessFully(CheckInResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos);

    void employeeCheckInFailure(String errorMessage);

}//CheckInResponseListener closes here.....
