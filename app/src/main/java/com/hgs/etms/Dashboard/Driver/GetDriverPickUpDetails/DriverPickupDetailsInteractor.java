package com.hgs.etms.Dashboard.Driver.GetDriverPickUpDetails;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.Dashboard.Driver.PickupDropDetailsModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

public class DriverPickupDetailsInteractor {

    //High priority variables goes below....
    private GetDriverPickUpDetailsListener pickupDetailsListener;
    private TRAVEL_TYPE_ENUM travelType;
    private Context mContext;

    //Least priority variables goes below....
    private final String TAG = "DriverPickupDetailsInteractor".toString().trim().substring(0,23);
    private final String getPickupDetailsUrl = "DriverPickup";//http://124.30.44.228/ETMSMOBILE.Api/api/DriverPickup


    public DriverPickupDetailsInteractor(GetDriverPickUpDetailsListener pickupDetailsListener, TRAVEL_TYPE_ENUM travelType, Context mContext) {
        this.pickupDetailsListener = pickupDetailsListener;
        this.travelType = travelType;
        this.mContext = mContext;
    }//DriverPickupDetailsInteractor constructor closes here......


    public void getTravelDetails(final Map<String, String> params){

        if(this.pickupDetailsListener == null)
            throw new NullPointerException("pickupDetailsListener cannot be null.");
        else {
            String apiUrl = BuildConfig.SERVER_URL + getPickupDetailsUrl;
            Log.d(TAG, "URL = " + apiUrl);
            Log.d(TAG, "Params = " + params);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, apiUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "Response is : " + response);


                            PickupDropDetailsModel dropDetailsModel = new Gson().fromJson(response, PickupDropDetailsModel.class);

                            ////////////...........PARSING RESPONSE..........\\\\\\\\\\\\\\
                            if (dropDetailsModel.status) {
                                //Status is true.
                                pickupDetailsListener.driverPickUpDetailsSuccess(dropDetailsModel, travelType);
                            }//if(travelDetailsModel.status) closes here....
                            else {
                                //Status is false.
                                pickupDetailsListener.driverPickUpDetailsFailure(dropDetailsModel.message);
                            }//else closes here....
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getActivity(), "ErrorListener \n " + error.toString(), Toast.LENGTH_LONG).show();
                            pickupDetailsListener.driverPickUpDetailsFailure(error.getMessage()+" "+error.toString());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");//TODO: Token is Hardcoded, need to softcode it.
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    return params;
                }

            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                    AppConstants.VOLLEY_MAX_RETRIES,
                    AppConstants.VOLLEY_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this.mContext);
            requestQueue.add(stringRequest);
        }//else listener is not null closes here......
    }//getTravelDetails closes here.....


}//DriverPickupDetailsInteractor closes here.....
