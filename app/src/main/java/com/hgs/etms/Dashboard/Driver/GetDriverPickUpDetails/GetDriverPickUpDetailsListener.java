package com.hgs.etms.Dashboard.Driver.GetDriverPickUpDetails;

import com.hgs.etms.Dashboard.Driver.PickupDropDetailsModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;

public interface GetDriverPickUpDetailsListener {

    /**
     * @travelType: This will tell whether user is travelling for Drop or Pickup
     * **/
    void driverPickUpDetailsSuccess(PickupDropDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType);

    void driverPickUpDetailsFailure(String errorMessage);

}//GetDriverPickUpDetailsListener closes here....
