package com.hgs.etms.Dashboard.Employee;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hgs.etms.Dashboard.Driver.PICKUP_DROP_TYPE_ENUM;
import com.hgs.etms.Dashboard.Driver.ViewDetails.GetPickDropDetailsClickHandler;
import com.hgs.etms.Dashboard.Driver.ViewDetails.GetPickupDropDetailsListener;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutClickHandleInteractor;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutResponseModel;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.Feedback.FeedbackDialogFragment;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.GetTravelDetails.TravelDetailsDialogFragment;
import com.hgs.etms.GetTravelDetails.TravelDetailsModel;
import com.hgs.etms.R;
import com.hgs.etms.ScanActivity;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.LocationTrack;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

class EmployeePickupDropAdapter extends RecyclerView.Adapter implements GetPickupDropDetailsListener, CheckOutClickHandleInteractor {


    //High priority variables goes below.....
    private List<GetAllTripsDataModel> mPickupDropTripsList;
    private final Activity mContext;
    private Boolean isToday;


    //Medium Priority goes here...
    private LocationTrack locationTrack;
    private double longitude, latitude;


    //Least priority variables goes below....
    private final String TAG = "PickupDropAdapter";
    private final int PICKUP_OR_LOGIN = 0, DROP_OR_LOGOUT = 1, VIEW_TYPE_FOOTER = 2 , VIEW_TYPE_CELL = 3;


    public EmployeePickupDropAdapter(Activity context, List<GetAllTripsDataModel> pickUpDropTripsList, Boolean isToday) {
        this.mContext = context;
        this.mPickupDropTripsList = pickUpDropTripsList;
        this.isToday = isToday;
        Collections.sort(this.mPickupDropTripsList, GetAllTripsDataModel.EMPLOYEE_PICKUP_DROP_SLOT_COMPARATOR);
        locationTrack = new LocationTrack(mContext);
    }//EmployeePickupDropAdapter constructor closes here.....

    @Override
    public int getItemViewType(int position) {

        if(position < mPickupDropTripsList.size()) {
            switch (PICKUP_DROP_TYPE_ENUM.valueOf((mPickupDropTripsList.get(position).slot_type.trim()))) {

                case LOGIN:
                    return PICKUP_DROP_TYPE_ENUM.LOGIN.getValue();

                case LOGOUT:
                    return PICKUP_DROP_TYPE_ENUM.LOGOUT.getValue();

                default:
                    return PICKUP_DROP_TYPE_ENUM.LOGIN.getValue();

            }//switch (TRAVEL_TYPE_ENUM.valueOf((mPickupDropTripsList.get(position).slot_type.trim())) closes here.....
        }//if(position <= mPickupDropTripsList.size()) closes here.....
        else{
            return (position == mPickupDropTripsList.size()) ? VIEW_TYPE_FOOTER : VIEW_TYPE_CELL;
        }//else closes here....
    }//getItemViewType closes here.....


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case DROP_OR_LOGOUT:
            case PICKUP_OR_LOGIN:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_login, parent, false);
                return new EmployeePickDropViewHolder(view, viewType);

            case VIEW_TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_feedback, parent, false);
                return new FeedbackViewHolder(view, viewType);

                default:
            case VIEW_TYPE_CELL:
                return null;

        }//switch (viewType) closes here....


    }//onCreateViewHolder closes here.....


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        TRAVEL_TYPE_ENUM traveltype = TRAVEL_TYPE_ENUM.PICKUP;//Bcoz depending on this we have to fetch travel details on show on the Alert Dialog.
        switch (holder.getItemViewType()) {

            case PICKUP_OR_LOGIN:
                //Show data for Pickup....
                EmployeePickDropViewHolder pickupHolder = (EmployeePickDropViewHolder) holder;
                traveltype = TRAVEL_TYPE_ENUM.PICKUP;
                setupDataFor(pickupHolder, position, traveltype);
                break;

            case DROP_OR_LOGOUT:
                //Show data for Drop....
                EmployeePickDropViewHolder dropholder = (EmployeePickDropViewHolder) holder;
                traveltype = TRAVEL_TYPE_ENUM.DROP;
                setupDataFor(dropholder, position, traveltype);
                break;

            case VIEW_TYPE_FOOTER:
                FeedbackViewHolder feedbackViewHolder = (FeedbackViewHolder) holder;
                feedbackViewHolder.mFeedbackBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //displayRatingAlert(0);
                    }//onClick closes here....
                });//feedbackViewHolder.mFeedbackBtn closes here.....
                break;

            case VIEW_TYPE_CELL:
                Log.w(TAG, "Unused VIEW_TYPE_CELL");
                break;
        }//switch (holder.getItemViewType()) closes here....

    }//onBindViewHolder closes here.....







    @Override
    public int getItemCount() {
        if (mPickupDropTripsList == null)
            return 0;
        else if(mPickupDropTripsList.isEmpty())
            return 0;
        else
            return mPickupDropTripsList.size()+1;//+1 bcoz we are adding Footer....
    }//getItemCount closes here.....



    private void setupDataFor(EmployeePickDropViewHolder pickupDropHolder, int position, TRAVEL_TYPE_ENUM traveltype){
//        Boolean isToday = mPickupDropTripsList.get(position).isToday;
        Boolean isCheckedIn = mPickupDropTripsList.get(position).is_checked_in;
        Boolean isCheckedOut = mPickupDropTripsList.get(position).is_checked_out;

        switch (traveltype){
            case DROP:

                pickupDropHolder.mPickupDropImageV.setImageResource(R.drawable.ic_car_logout);
                pickupDropHolder.mPickupDropImageV.setBackgroundResource(R.drawable.drop_bg_patch);

                if(isToday){
                    pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.todaysDropSpelling));

                    if(!isCheckedIn && !isCheckedOut){//0 0
                        //Unknown condition....
                        showCheckIn(pickupDropHolder.mBtnStartStopTrip);
                    }//if(!isCheckedIn && !isCheckedOut) closes here....
                    else if(!isCheckedIn && isCheckedOut){//0 1
                        //trip is checked Out....i.e. completed.
                        showCompleted(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(!isCheckedIn && isCheckedOut) closes here....
                    else if(isCheckedIn && !isCheckedOut){//1 0
                        //trip is already checked in....
                        showCheckOut(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(isCheckedIn && !isCheckedOut) closes here....
                    else if(isCheckedIn && isCheckedOut){//1 1
                        showCompleted(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(isCheckedIn && isCheckedOut) closes here....
                    else{
                        Log.w(TAG, "Unhandled condition, where isCheckedIn = "+isCheckedIn+" & isCheckedOut = "+isCheckedOut);
                        showCheckIn(pickupDropHolder.mBtnStartStopTrip);
                    }//else closes here....

                }//if(isToday) closes here....
                else{

                    SimpleDateFormat formatter =AppConstants.YYYY_MM_DD_DATEFORMAT;
                    String format = formatter.format(new Date());
                    System.out.println(format);

                    String scheduledDate = mPickupDropTripsList.get(position).scheduled_date;

                    try {
                       //  Date currentDate= AppConstants.YYYY_MM_DD_DATEFORMAT.parse("2019-10-26");
                        Date currentDate= AppConstants.YYYY_MM_DD_DATEFORMAT.parse(format); //TODO remove comment for actual date
                        Date scheduleDate=AppConstants.YYYY_MM_DD_DATEFORMAT.parse(scheduledDate);


                            pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.dropSpelling));
                            pickupDropHolder.mBtnStartStopTrip.setVisibility(View.VISIBLE);
                            pickupDropHolder.mBtnStartStopTrip.setEnabled(false);
                            pickupDropHolder.mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.transparentGrey));

                            if(scheduleDate.before(currentDate)){
                                if(mPickupDropTripsList.get(position).is_checked_in&&mPickupDropTripsList.get(position).is_checked_out) {
                                    pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.completedSpelling));
                                }else {
                                    pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.expiredSpelling));
                                }
                            }else {
                                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.upComingSpelling));
                            }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }//else closes here.....
                break;



            case PICKUP:
                pickupDropHolder.mPickupDropImageV.setImageResource(R.drawable.ic_car_login);
                pickupDropHolder.mPickupDropImageV.setBackgroundResource(R.drawable.pickup_bg_patch);

//                showCheckOut(pickupDropHolder.mBtnStartStopTrip);;//TODO: COMMENT WHILE MERGING CODE TO MASTER....
                if(isToday) {
                    pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.todaysPickupSpelling));

                    if(!isCheckedIn && !isCheckedOut){//0 0
                        //Unknown condition....
                        showCheckIn(pickupDropHolder.mBtnStartStopTrip);
                    }//if(!isCheckedIn && !isCheckedOut) closes here....
                    else if(!isCheckedIn && isCheckedOut){//0 1
                        //trip is checked Out....i.e. completed.
                        showCompleted(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(!isCheckedIn && isCheckedOut) closes here....
                    else if(isCheckedIn && !isCheckedOut){//1 0
                        //trip is already checked in....
//                        showCheckOut(pickupDropHolder.mBtnStartStopTrip);
                        showInProgress(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(isCheckedIn && !isCheckedOut) closes here....
                    else if(isCheckedIn && isCheckedOut){//1 1
                        showCompleted(pickupDropHolder.mBtnStartStopTrip);
                    }//else if(isCheckedIn && isCheckedOut) closes here....
                    else{
                        Log.w(TAG, "Unhandled condition, where isCheckedIn = "+isCheckedIn+" & isCheckedOut = "+isCheckedOut);
                        showCheckIn(pickupDropHolder.mBtnStartStopTrip);
                    }//else closes here....
                }//if(isToday) closes here.....
                else{

                    SimpleDateFormat formatter = AppConstants.YYYY_MM_DD_DATEFORMAT;
                    String format = formatter.format(new Date());
                    System.out.println(format);

                    String scheduledDate = mPickupDropTripsList.get(position).scheduled_date;

                    try {
                       // Date currentDate= AppConstants.YYYY_MM_DD_DATEFORMAT.parse("2019-10-26");
                          Date currentDate= AppConstants.YYYY_MM_DD_DATEFORMAT.parse(format); //TODO remove comment for actual date
                        Date scheduleDate= AppConstants.YYYY_MM_DD_DATEFORMAT.parse(scheduledDate);

                        pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.pickupSpelling));
                        pickupDropHolder.mBtnStartStopTrip.setVisibility(View.VISIBLE);
                        pickupDropHolder.mBtnStartStopTrip.setEnabled(false);
                        pickupDropHolder.mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.transparentGrey));

                        if(scheduleDate.before(currentDate)){

                            if(mPickupDropTripsList.get(position).is_checked_in&&mPickupDropTripsList.get(position).is_checked_out) {
                                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.completedSpelling));
                            }else {
                                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.expiredSpelling));
                            }

                        }else {
                            pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.upComingSpelling));

                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }



                }//else closes here....
                break;
        }//switch (traveltype) closes here...




        pickupDropHolder.mPickUpDropTimeDateTxtV.setText(mPickupDropTripsList.get(position).slot + " | " + mPickupDropTripsList.get(position).scheduled_date);

        //View Details goes below.....
        pickupDropHolder.mViewDetailsTxtV.setText(mContext.getString(R.string.viewDetailSpelling));
        pickupDropHolder.mViewDetailsTxtV.setOnClickListener(new GetPickDropDetailsClickHandler(pickupDropHolder.mViewDetailsTxtV.getContext()
                , this, this.mPickupDropTripsList.get(position), position, traveltype));


        pickupDropHolder.mBtnStartStopTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //If user is able to click, then ofcourse it is today....
                getLocation();

                if (!isCheckedIn && !isCheckedOut) {//0 0
                    //Unknown condition....
                    Intent intentScan = new Intent(pickupDropHolder.mBtnStartStopTrip.getContext(), ScanActivity.class);
                    intentScan.putExtra(AppConstants.TRAVEL_DETAILS_OBJECT_KEY, mPickupDropTripsList.get(position));
                    intentScan.putExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, position);
                    intentScan.putExtra("scanType", 0);
                    intentScan.putExtra(AppConstants.EMPLOYEE_LATITUDE, String.valueOf(latitude));
                    intentScan.putExtra(AppConstants.EMPLOYEE_LONGITUDE, String.valueOf(longitude));
                    (pickupDropHolder.mBtnStartStopTrip.getContext()).startActivity(intentScan);
                }//if(!isCheckedIn && !isCheckedOut) closes here....
                else if (!isCheckedIn && isCheckedOut) {//0 1
                    //trip is checked Out....i.e. completed.
                    Log.w(TAG, "This case should have been unclicked, where isCheckedIn = " + isCheckedIn + " & isCheckedOut = " + isCheckedOut);
                }//else if(!isCheckedIn && isCheckedOut) closes here....
                else if (isCheckedIn && !isCheckedOut) {//1 0
//                    trip is already checked in....Checkout API should be called here....
                    Intent intentScan = new Intent(pickupDropHolder.mBtnStartStopTrip.getContext(), ScanActivity.class);
                    intentScan.putExtra(AppConstants.TRAVEL_DETAILS_OBJECT_KEY, mPickupDropTripsList.get(position));
                    intentScan.putExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, position);
                    intentScan.putExtra(AppConstants.EMPLOYEE_LATITUDE, String.valueOf(latitude));
                    intentScan.putExtra(AppConstants.EMPLOYEE_LONGITUDE, String.valueOf(longitude));
                    intentScan.putExtra("scanType", 1);
                    (pickupDropHolder.mBtnStartStopTrip.getContext()).startActivity(intentScan);
                }//else if(isCheckedIn && !isCheckedOut) closes here....
                else if (isCheckedIn && isCheckedOut) {//1 1
                    Log.w(TAG, "This case should have been unclicked, where isCheckedIn = " + isCheckedIn + " & isCheckedOut = " + isCheckedOut);
                }//else if(isCheckedIn && isCheckedOut) closes here....
                else {
                    Log.w(TAG, "Unhandled condition, where isCheckedIn = " + isCheckedIn + " & isCheckedOut = " + isCheckedOut);
                }//else closes here....
            }//onClick closes here....
        });//View.OnClickListener() closes here....

    }//setupDataFor closes here.....

    private void showCheckOut(Button mBtnStartStopTrip) {
        mBtnStartStopTrip.setVisibility(View.VISIBLE);
        mBtnStartStopTrip.setText(mContext.getString(R.string.checkOutSpelling));
        mBtnStartStopTrip.setEnabled(true);
        mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
    }//showCheckOut closes here....

    private void showCheckIn(Button mBtnStartStopTrip) {
        mBtnStartStopTrip.setVisibility(View.VISIBLE);
        mBtnStartStopTrip.setText(mContext.getString(R.string.checkInSpelling));
        mBtnStartStopTrip.setEnabled(true);
        mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
    }//showCheckIn closes here....


    private void showCompleted(Button mBtnStartStopTrip) {
        mBtnStartStopTrip.setVisibility(View.VISIBLE);
        mBtnStartStopTrip.setText(mContext.getString(R.string.completedSpelling));
        mBtnStartStopTrip.setEnabled(false);
        mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.transparentGrey));
    }//showCompleted closes here....

    private void showInProgress(Button mBtnStartStopTrip){
        mBtnStartStopTrip.setVisibility(View.VISIBLE);
        mBtnStartStopTrip.setText(mContext.getString(R.string.inProgressSpelling));
        mBtnStartStopTrip.setEnabled(false);
        mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.transparentGrey));
    }//showInProgress closes here.....


    /**
     * When user clicks on View DetAils, the API is called & we get respnse here....
     **/
    @Override
    public void pickupDropdetailsFetchedSuccessfully(TravelDetailsModel travelDetailsModel, int clickedPosition, TRAVEL_TYPE_ENUM traveltype) {
       // Toast.makeText(mContext, travelDetailsModel.message, Toast.LENGTH_SHORT).show();


        String userType =   PreferenceManager.getDefaultSharedPreferences(mContext).getString(AppConstants.SF_LOGGED_IN_USER_TYPE, "");

        Bundle detailsExtras = new Bundle();
        detailsExtras.putParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY, travelDetailsModel);
        detailsExtras.putSerializable(AppConstants.TRAVEL_TYPE_KEY, traveltype);
        detailsExtras.putString(AppConstants.SF_LOGGED_IN_USER_TYPE, userType);



        String backstackTAG = "PickUpDropDetailsAlertDialog";
        TravelDetailsDialogFragment detailsFragDialog = new TravelDetailsDialogFragment();
        detailsFragDialog.setArguments(detailsExtras);
        FragmentTransaction fragmentTransaction = ((Activity) mContext).getFragmentManager().beginTransaction();//getSupportFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(backstackTAG);
        detailsFragDialog.show(fragmentTransaction, backstackTAG);
    }//pickupDropdetailsFetchedSuccessfully closes here.....



    private void displayFeedbackAlert(GetAllTripsDataModel getAllTripsDataModel){

        Bundle detailsExtras = new Bundle();
        detailsExtras.putParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY,getAllTripsDataModel );

        String backstackTAG = "FeedbackDialog";
        FeedbackDialogFragment detailsFragDialog = new FeedbackDialogFragment();
        detailsFragDialog.setArguments(detailsExtras);
        FragmentTransaction fragmentTransaction = mContext.getFragmentManager().beginTransaction();//getSupportFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(null);
        detailsFragDialog.show(fragmentTransaction, backstackTAG);
    }

 //   private void displayRatingAlert(GetAllTripsDataModel allTripsDataModel) {




//        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
//        View dialogLayout = inflater.inflate(R.layout.rating_alert, null);
//
//
//        final RatingBar ratingBar = dialogLayout.findViewById(R.id.ratingBar);
//        builder.setView(dialogLayout);
//        builder.setCancelable(true);
//        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
////                Toast.makeText(getActivity(), "Thanks for your Feedback" + ratingBar.getRating(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(mContext, "Thanks for your Feedback", Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.show();



    /**
     * When user clicks on View  details, & due to some error we are not able to fetch details, then control comes here..
     **/
    @Override
    public void pickupDropDetailsFetchedFailure(String errorMessage) {
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
    }//pickupDropDetailsFetchedFailure closes here.....

    @Override
    public void checkedOutSuccessfully(String responseMessage, CheckOutResponseModel checkOutResponseModel, int position) {
        Toast.makeText(mContext,responseMessage,Toast.LENGTH_LONG).show();

        GetAllTripsDataModel tripDataModel = mPickupDropTripsList.get(position);
        tripDataModel.is_checked_out = true;
        tripDataModel.is_checked_in = false;

        mPickupDropTripsList.remove(position);
        mPickupDropTripsList.add(position, tripDataModel);



        notifyItemChanged(position);


    }//checkedOutSuccessfully closes here....

    @Override
    public void checkOutFailure(String errorMessage) {
        Toast.makeText(mContext,errorMessage,Toast.LENGTH_LONG).show();
    }


    ////////////............VIEWHOLDERS GOES BELOW...........\\\\\\\\\\\\\\
    private class EmployeePickDropViewHolder extends RecyclerView.ViewHolder {

        //High priority UI variables goes below.....
        private TextView mPickUpDropheadingTxtV, mPickUpDropTimeDateTxtV, mViewDetailsTxtV;
        private Button mBtnStartStopTrip;

        private ImageView mPickupDropImageV;
        private LinearLayout mPickupParentContainer;


        public EmployeePickDropViewHolder(View view, int viewType) {
            super(view);

            mPickUpDropheadingTxtV = view.findViewById(R.id.tvPickupDropHeading);
            mPickUpDropTimeDateTxtV = view.findViewById(R.id.tvPickupDropTimeDate);
            mViewDetailsTxtV = view.findViewById(R.id.tvViewDetails);
            mBtnStartStopTrip = view.findViewById(R.id.btnStartStopPickup);

            mPickupDropImageV = view.findViewById(R.id.imgPickupDropV);
            mPickupParentContainer = view.findViewById(R.id.pickupContainer);
        }//EmployeePickDropViewHolder constructor closes here.....
    }//EmployeePickDropViewHolder class closes here.....



    private class FeedbackViewHolder extends RecyclerView.ViewHolder {

        //High priority UI variables goes below.....
        private ImageView mFeedbackBtn;


        public FeedbackViewHolder(View view, int viewType) {
            super(view);

            mFeedbackBtn = view.findViewById(R.id.feedbackBtn);
        }//EmployeePickDropViewHolder constructor closes here.....
    }//EmployeePickDropViewHolder class closes here.....


    private void getLocation() {
        locationTrack = new LocationTrack(mContext);

        if (locationTrack.canGetLocation()) {
            longitude = locationTrack.getLongitude();
            latitude = locationTrack.getLatitude();
        } else {
            locationTrack.showSettingsAlert();
        }
    }//getLocation closes here....


}//EmployeePickupDropAdapter closes here....
