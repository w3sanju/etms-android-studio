package com.hgs.etms.Dashboard.Employee;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hgs.etms.Dashboard.Employee.DashboardViewPager.CustomPagerModel;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.GetTravelDetails.GetTravelDetailsListener;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.GetTravelDetails.TravelDetailsDialogFragment;
import com.hgs.etms.GetTravelDetails.TravelDetailsModel;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import android.support.v4.content.ContextCompat;

public class EmployeeDashboardFragment extends Fragment implements GetTravelDetailsListener {

    //High priority UI variables goes below....
    private View view;
    private RecyclerView mPickupDropRecyclerView;
    private TextView mNoDataFoundErrorView;
    private EmployeePickupDropAdapter pickupDropAdapter;


    //Medium priority NON-UI variables goes below....
    private ArrayList<GetAllTripsDataModel> loginTripsOnDate, logoutTripsOnDate;
    private String fragmentForDate = "";//Since there are dynamic fragments, in the Viewpager, therefore this is the date for current fragment.
    private BroadcastReceiver mEmployeeCheckInReciever, mEmployeeCheckOutReciever;
    private IntentFilter mEmplyeeCheckinIntentFilter, mEmplyeeCheckOutIntentFilter;
    private List<GetAllTripsDataModel> listOfAdapter;//This list contains both Checkin & Chekout both merged lists.
    private boolean isTodayVal;

    //Least priority variables goes below.....
    private final String TAG = "EmployeeDashboardFragment".toString().substring(0, 23);


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Location permission granted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Location permission denied", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard_employee, container, false);


        //Step:1 >> Initializations.....
        initializations();



        //Step:2 >> fetch data & date from Arguments & call the API to get data for corresponding date....
        if(getArguments() != null){
            if(getArguments().getParcelable(AppConstants.EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY) != null){
                CustomPagerModel pagerModel = getArguments().getParcelable(AppConstants.EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY);
                fragmentForDate = pagerModel.getDate();
                isTodayVal = pagerModel.isToday();


                loginTripsOnDate = getArguments().getParcelableArrayList(AppConstants.EMPLOYEE_LOGIN_TRIPS_KEY);//This lsit will contain trip for above date.
                logoutTripsOnDate =  getArguments().getParcelableArrayList(AppConstants.EMPLOYEE_LOGOUT_TRIPS_KEY);//This lsit will contain trip for above date.

            }//if(getArguments().getParcelable(AppConstants.EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY) != null) closes here....
            else{
                Log.w(TAG , "getArguments().getParcelable(AppConstants.EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY) is null.");
            }//else closes here....
        }//if(getArguments() != null) closes here,.....
        else{
            Log.w(TAG, "getArguments is null.");
        }//else closes here....



        //Step:3 >> Setting up the Adapter of trips....
        setupDetailsOnUI();


        return view;
    }

    @Override
    public void onResume() {

        if(isTodayVal) {
            ////////..........CHECKIN BROADCAST..........\\\\\\\\\\\\
            mEmplyeeCheckinIntentFilter = new IntentFilter(AppConstants.EMPLOYEE_CHECK_IN_BROADCAST);
            mEmployeeCheckInReciever = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
//                String checkinStatus = intent.getStringExtra("CheckinStatus");
                    int position = intent.getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, 0);

                    if (listOfAdapter != null) {
                        if (listOfAdapter.size() > position) {
                            GetAllTripsDataModel allTripsDataModel = listOfAdapter.get(position);
                            listOfAdapter.remove(position);

                            allTripsDataModel.is_checked_in = true;
                            allTripsDataModel.is_checked_out = false;

                            listOfAdapter.add(position, allTripsDataModel);
                            pickupDropAdapter.notifyDataSetChanged();
                        }//if (listOfAdapter.size() > position) closes here....
                        else
                            Log.w(TAG, "listOfAdapter.size() < position...");
                    }//if(listOfAdapter != null) closes here.....
                    else
                        Log.w(TAG, "lisOfAdapter is null.");
                }//onReceive closes here....
            };
            getActivity().registerReceiver(mEmployeeCheckInReciever, mEmplyeeCheckinIntentFilter);


            /////////.........CHECKOUT BROADCAST........\\\\\\\\\\\\
            mEmplyeeCheckOutIntentFilter = new IntentFilter(AppConstants.EMPLOYEE_CHECKOUT_BROADCAST);
            mEmployeeCheckOutReciever = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
//                String checkinStatus = intent.getStringExtra("CheckinStatus");
                    int position = intent.getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, 0);

                    if (listOfAdapter != null) {
                        if (listOfAdapter.size() > position) {
                            GetAllTripsDataModel allTripsDataModel = listOfAdapter.get(position);
                            listOfAdapter.remove(position);

                            allTripsDataModel.is_checked_in = false;
                            allTripsDataModel.is_checked_out = true;

                            listOfAdapter.add(position, allTripsDataModel);
                            pickupDropAdapter.notifyDataSetChanged();
                        }//if (listOfAdapter.size() > position) closes her.....
                        else
                            Log.w(TAG, "listOfAdapter.size() < position...");
                    }//if(listOfAdapter != null) closes here.....
                    else
                        Log.w(TAG, "lisOfAdapter is null.");
                }//onReceive closes here....
            };
            getActivity().registerReceiver(mEmployeeCheckOutReciever, mEmplyeeCheckOutIntentFilter);

        }//if(isTodayVal) closes here....
        else
            Log.d(TAG, "Not Today.");







        super.onResume();

    }//onResume closes here.....

//    @Override
//    public void () {
//        getActivity().unregisterReceiver(mEmployeeCheckInReciever);
//        super.onPause();
//    }//onPause closes here.....


    @Override
    public void onDestroy() {
        if(mEmployeeCheckInReciever != null)
            getActivity().unregisterReceiver(mEmployeeCheckInReciever);

        if(mEmployeeCheckOutReciever != null)
            getActivity().unregisterReceiver(mEmployeeCheckOutReciever);

        super.onDestroy();
    }

    private void initializations() {
        mPickupDropRecyclerView = view.findViewById(R.id.recycViewPickupDrop);
        mNoDataFoundErrorView = view.findViewById(R.id.noDataFoundTxtView);
    }//initializations closes above....


    /**
     * This function will setup all the Trip on the UI, i.e. all the fwd & return trips in a list.
     * **/
    private void setupDetailsOnUI(){
        listOfAdapter = new ArrayList<>();
        listOfAdapter.addAll(loginTripsOnDate);
        listOfAdapter.addAll(logoutTripsOnDate);

//        for (int i=0; i<listOfAdapter.size();i++){
//            listOfAdapter.get(i).isToday = isTodayValue;
//        }

        if(listOfAdapter.isEmpty()){
            //Oops no data found for this Fragment/Date....
            mNoDataFoundErrorView.bringToFront();
            mNoDataFoundErrorView.setText(getString(R.string.noTripFoundErrorMessage)+" "+fragmentForDate);
            mNoDataFoundErrorView.setVisibility(View.VISIBLE);
            mPickupDropRecyclerView.setVisibility(View.GONE);
        }//if(allTripsDataModelsList.isEmpty()) closes here....
        else{
            pickupDropAdapter = new EmployeePickupDropAdapter(getActivity(), listOfAdapter, isTodayVal);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mPickupDropRecyclerView.setLayoutManager(mLayoutManager);
            mPickupDropRecyclerView.setAdapter(pickupDropAdapter);
            mPickupDropRecyclerView.bringToFront();
            mNoDataFoundErrorView.setVisibility(View.GONE);
            mPickupDropRecyclerView.setVisibility(View.VISIBLE);
        }//else closes here....
    }//setupDetailsOnUI closes here.....


    @Override

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {

            if (result.getContents() == null) {
                Log.d("scan", "cancelled");
                Toast.makeText(getActivity(), "cancelled: ", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);

        }

    }






    ////////////////........... OVERRIDED RESPONSE LISTENERS GOES BELOW..............\\\\\\\\\\\\\\\
    @Override
    public void travelDetailsSuccess(TravelDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType) {

        Bundle detailsExtras = new Bundle();
        detailsExtras.putParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY, travelDetailsModel);
        detailsExtras.putSerializable(AppConstants.TRAVEL_TYPE_KEY, travelType);

        String backstackTAG = "PickUpDropDetailsAlertDialog";
        TravelDetailsDialogFragment detailsFragDialog = new TravelDetailsDialogFragment();
        detailsFragDialog.setArguments(detailsExtras);
        FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();//getSupportFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(backstackTAG);
        detailsFragDialog.show(fragmentTransaction, backstackTAG);

    }//travelDetailsSuccess closes here......

    @Override
    public void travelDetailsFailure(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }//travelDetailsFailure closes here......

}
