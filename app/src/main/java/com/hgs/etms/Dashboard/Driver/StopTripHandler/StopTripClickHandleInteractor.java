package com.hgs.etms.Dashboard.Driver.StopTripHandler;

public interface StopTripClickHandleInteractor {

    void tripStoppedSuccessfully(String responseMessage, StopTripResponseModel stopTripModel, int position);

    void tripStoppedFailure(String errorMessage);
}
