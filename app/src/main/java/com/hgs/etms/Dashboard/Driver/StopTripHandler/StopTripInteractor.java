package com.hgs.etms.Dashboard.Driver.StopTripHandler;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

public class StopTripInteractor {
    //High priority variables goes below.....
    private StopTripResponseListener stopTripListener;
    private TRAVEL_TYPE_ENUM travelType;

    //Medium Priority Variable
    private Context mContext;
    private Integer mPos;


    //Least priority variables goes below.....
    private final String TAG = "StopTripInteractor";
    private final String stopTripUrl = "EndTrip";//http://124.30.44.228/ETMSMOBILE.Api/api/EndTrip

    public StopTripInteractor(StopTripResponseListener stopTripListener, TRAVEL_TYPE_ENUM travelType, Context context) {
        this.stopTripListener = stopTripListener;
        this.travelType = travelType;
        mContext = context;

    }//constructor closes here....


    public void execute(final Map<String, String> params, Integer pos){
        mPos = pos;
        if(this.stopTripListener == null)
            throw new NullPointerException("pickupDetailsListener cannot be null.");
        else {
            String apiUrl = BuildConfig.SERVER_URL + stopTripUrl;
            Log.d(TAG, "URL = " + apiUrl);
            Log.d(TAG, "Params = " + params);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, apiUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "Response is : " + response);


                            StopTripResponseModel stoptTripResponseModel = new Gson().fromJson(response, StopTripResponseModel.class);

                            ////////////...........PARSING RESPONSE..........\\\\\\\\\\\\\\
                            if (stoptTripResponseModel.status) {
                                //Status is true.
                                stopTripListener.driverStoppedTripSuccessfully(stoptTripResponseModel, travelType,mPos);
                            }//if(stopTripResponseModel.status) closes here....
                            else {
                                //Status is false.
                                stopTripListener.driverStoppedTripFailure(stoptTripResponseModel.message);
                            }//else closes here....
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getActivity(), "ErrorListener \n " + error.toString(), Toast.LENGTH_LONG).show();
                            stopTripListener.driverStoppedTripFailure(error.getMessage()+" "+error.toString());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");//TODO: Token is Hardcoded, need to softcode it.
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                    AppConstants.VOLLEY_MAX_RETRIES,
                    AppConstants.VOLLEY_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this.mContext);
            requestQueue.add(stringRequest);
        }//else listener is not null closes here......
    }

}//StopTripInteractor closes here.....
