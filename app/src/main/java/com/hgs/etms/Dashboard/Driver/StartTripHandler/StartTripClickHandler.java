package com.hgs.etms.Dashboard.Driver.StartTripHandler;

import android.content.Context;
import android.view.View;

import com.hgs.etms.Dashboard.Driver.PickupDropDataModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.Apputils;

import java.util.HashMap;
import java.util.Map;


public class StartTripClickHandler implements View.OnClickListener, StartTripResponseListener {


    //Medium Priority Non-UI variables goes below
    private PickupDropDataModel mPickupDropDataModel;
    private Context context;
    private StartTripClickHandleInteractor mClickResponseHandler;
    private Integer pos;

    public StartTripClickHandler(PickupDropDataModel pickupDropDataModel, int position, Context mContext, StartTripClickHandleInteractor clickResponseHandler) {

        mPickupDropDataModel = pickupDropDataModel;
        context = mContext;
        this.mClickResponseHandler = clickResponseHandler;
        pos = position;
    }

    @Override
    public void onClick(View view) {
        Apputils.showConfirmationStartStopTrip(context.getString(R.string.alert_title),context.getString(R.string.confirmation_start_trip),context,StartTripClickHandler.this);
    }

    @Override
    public void driverStartedTripSuccessfully(StartTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        if (responseModel.status)//if true...
            mClickResponseHandler.tripStartedSuccessfully(responseModel.message, responseModel, pos);
        else
            mClickResponseHandler.tripStartedFailure(responseModel.message);
    }

    @Override
    public void driverStartedTripFailure(String errorMessage) {
        mClickResponseHandler.tripStartedFailure(errorMessage);
    }

   public void startTripClick(){

       Map<String, String> startTripParams = new HashMap<>();
       startTripParams.put(AppConstants.SCHEDULED_DATE_KEY, mPickupDropDataModel.scheduled_date);
       startTripParams.put(AppConstants.VEHICLE_ID, mPickupDropDataModel.vehicle_id);
       startTripParams.put(AppConstants.SLOT_KEY, mPickupDropDataModel.slot);
       startTripParams.put(AppConstants.DRIVER_NAME_KEY, mPickupDropDataModel.driver_name);
       startTripParams.put(AppConstants.SLOT_TYPE_CAMELCASE_KEY, mPickupDropDataModel.slot_type);


       StartTripInteractor startTripInteractor = new StartTripInteractor(this, TRAVEL_TYPE_ENUM.DROP, context);
       startTripInteractor.execute(startTripParams, pos);
   }
}
