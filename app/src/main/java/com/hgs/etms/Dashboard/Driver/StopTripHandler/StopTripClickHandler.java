package com.hgs.etms.Dashboard.Driver.StopTripHandler;

import android.content.Context;
import android.view.View;

import com.hgs.etms.Dashboard.Driver.PickupDropDataModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.Apputils;

import java.util.HashMap;
import java.util.Map;


public class StopTripClickHandler implements View.OnClickListener, StopTripResponseListener {


    //Medium Priority Non-UI variables goes below
    private PickupDropDataModel mPickupDropDataModel;
    private Context context;
    private StopTripClickHandleInteractor mClickResponseHandler;
    private Integer pos;


    public StopTripClickHandler(PickupDropDataModel pickupDropDataModel, int position, Context mContext, StopTripClickHandleInteractor clickResponseHandler) {
        this.mPickupDropDataModel = pickupDropDataModel;
        this.context = mContext;
        this.mClickResponseHandler = clickResponseHandler;
        this.pos = position;
    }

    @Override
    public void onClick(View view) {
        Apputils.showConfirmationStartStopTrip(context.getString(R.string.alert_title),context.getString(R.string.confirmation_stop_trip),context,StopTripClickHandler.this);
    }


    @Override
    public void driverStoppedTripSuccessfully(StopTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        if (responseModel.status)//if true
            mClickResponseHandler.tripStoppedSuccessfully(responseModel.message, responseModel, pos);
        else
            mClickResponseHandler.tripStoppedFailure(responseModel.message);
    }

    @Override
    public void driverStoppedTripFailure(String errorMessage) {
        mClickResponseHandler.tripStoppedFailure(errorMessage);
    }

    public void stopTripClick(){
        Map<String, String> stopTripParams = new HashMap<>();
        stopTripParams.put(AppConstants.SCHEDULED_DATE_KEY, mPickupDropDataModel.scheduled_date);
        stopTripParams.put(AppConstants.VEHICLE_ID, mPickupDropDataModel.vehicle_id);
        stopTripParams.put(AppConstants.SLOT_KEY, mPickupDropDataModel.slot);
        stopTripParams.put(AppConstants.DRIVER_NAME_KEY, mPickupDropDataModel.driver_name);
        stopTripParams.put(AppConstants.SLOT_TYPE_CAMELCASE_KEY, mPickupDropDataModel.slot_type);

        StopTripInteractor stopTripInteractor = new StopTripInteractor(this, TRAVEL_TYPE_ENUM.DROP, context);
        stopTripInteractor.execute(stopTripParams, pos);
    }
}
