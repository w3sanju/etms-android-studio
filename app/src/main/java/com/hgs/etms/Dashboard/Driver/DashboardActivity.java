package com.hgs.etms.Dashboard.Driver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hgs.etms.Dashboard.Driver.GetDriverDropDetails.DriverDropDetailsInteractor;
import com.hgs.etms.Dashboard.Driver.GetDriverDropDetails.GetDriverDropDetailsListener;
import com.hgs.etms.Dashboard.Driver.GetDriverPickUpDetails.DriverPickupDetailsInteractor;
import com.hgs.etms.Dashboard.Driver.GetDriverPickUpDetails.GetDriverPickUpDetailsListener;
import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripResponseListener;
import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripResponseModel;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripResponseListener;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripResponseModel;
import com.hgs.etms.Dashboard.Employee.EmployeeDashboardBaseActivity;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.Logout.Logout;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.Apputils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class DashboardActivity extends AppCompatActivity implements GetDriverDropDetailsListener, GetDriverPickUpDetailsListener, StartTripResponseListener, StopTripResponseListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    //High priority UI variables goes below.....
//    private TextView mTxtVEmpPickup, mTxtVEmpDrop;
//    private Button mStartStopPickupBtn, mStartStopDropBtn;
    private ScrollView mScrollContainer;
    private RecyclerView mPickupDropRecyclerView;
    private SwipeRefreshLayout pullToRefresh;
    ProgressDialog progressDialog;
    //Medium priority NON-UI variables goes below.....
    private List<PickupDropDataModel> pickUpDropTripsList;//This list will have all the pickup & drop of the driver.


    //Least priority NON-UI variables goes below....
    private final String TAG = "DashboardActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);


        mPickupDropRecyclerView = findViewById(R.id.recycViewPickupDrop);
        ImageButton logoutBtn = findViewById(R.id.logoutBtn);
        TextView username = findViewById(R.id.username);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(this);
        logoutBtn.setOnClickListener(this);

        //Changing the PullToRefresh color....
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimaryDark));

        final String employeeName = PreferenceManager.getDefaultSharedPreferences(this).getString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, "");
        username.setText(employeeName);


        getPickUpDetails(true);

    }//onCreate closes here......





    void getPickUpDetails(Boolean isShow){
        ///////////..........WHENEVER SCREEN LOADS, WE WILL GET THE DATA FOR PICKUP & DROP AVAILABLE FOR DRIVER..........\\\\\\\\\\\\\
        //01. LOAD PICKUP TRIPS....

        if(isShow) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Fetching Trips...");
            progressDialog.show();
        }
        Map<String, String> pickupParams = new HashMap<String, String>();
        pickupParams.put(AppConstants.SCHEDULED_DATE_KEY, new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date()));//TODO: Schedule date wILL BE MANIPULATED AFTER SANJU'S LOGIC COPLETION.
//        pickupParams.put(AppConstants.SCHEDULED_DATE_KEY, "06/08/2019");//TODO: TEMP CODE REMOVE....
        pickupParams.put(AppConstants.DRIVER_NAME_KEY, PreferenceManager.getDefaultSharedPreferences(this).getString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, ""));
        pickupParams.put(AppConstants.SLOT_TYPE_KEY, AppConstants.SLOT_TYPE_LOGIN_VALUE);

        DriverPickupDetailsInteractor pickupEmpInteractor = new DriverPickupDetailsInteractor(this, TRAVEL_TYPE_ENUM.PICKUP, this);
        pickupEmpInteractor.getTravelDetails(pickupParams);
    }//getPickUpDetails closes here....




    public void getDropDetails() {
        //02. LOAD DROP TRIPS.....
        Map<String, String> dropParams = new HashMap<String, String>();
                dropParams.put(AppConstants.SCHEDULED_DATE_KEY, new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date()));//TODO: Schedule date wILL BE MANIPULATED AFTER SANJU'S LOGIC COPLETION.
//        dropParams.put(AppConstants.SCHEDULED_DATE_KEY, "06/08/2019");//TODO: TEMP CODE REMOVE....
        dropParams.put(AppConstants.DRIVER_NAME_KEY, PreferenceManager.getDefaultSharedPreferences(this).getString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, ""));
        dropParams.put(AppConstants.SLOT_TYPE_KEY, AppConstants.SLOT_TYPE_LOGOUT_VALUE);

        DriverDropDetailsInteractor dropEmpInteractor = new DriverDropDetailsInteractor(this, TRAVEL_TYPE_ENUM.DROP, this);
        dropEmpInteractor.getTravelDetails(dropParams);
    }//getDropDetails closes here....



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.side_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Toast.makeText(this, "Selected Item: " + item.getTitle(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.logout:
                new Logout(this).execute();
                return true;
            case R.id.home:
                Intent intentHome = new Intent(this, EmployeeDashboardBaseActivity.class);
                startActivity(intentHome);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * This function will setup all the Trip on the UI, i.e. all the fwd & return trips in a list.
     **/
    private void setupDetailsOnUI(List<PickupDropDataModel> pickUpDropTripsList) {
        PickupDropAdapter pickupDropAdapter = new PickupDropAdapter(this, pickUpDropTripsList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mPickupDropRecyclerView.setLayoutManager(mLayoutManager);
        mPickupDropRecyclerView.setAdapter(pickupDropAdapter);
    }//setupDetailsOnUI closes here.....


    ////////////.............API CALLS GOES BELOW.............\\\\\\\\\\\\\\\\\
    @Override
    public void driverDropDetailsSuccess(PickupDropDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType) {

         if(progressDialog!=null)
             progressDialog.dismiss();

        if (pickUpDropTripsList == null)
            pickUpDropTripsList = new ArrayList<PickupDropDataModel>();


        pickUpDropTripsList.addAll(travelDetailsModel.data);


//        for (int i = 0; i < pickUpDropTripsList.size(); i++) {
//            pickUpDropTripsList.get(i).isTripStarted = false;
//        }  //TODO: for temporory purpose  isTripStarted is Hardcoded

        setupDetailsOnUI(pickUpDropTripsList);


    }//driverDropDetailsSuccess closes here.....

    @Override
    public void driverDropDetailsFailure(String errorMessage) {

        if(progressDialog!=null)
        progressDialog.dismiss();

        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }//driverDropDetailsFailure closes here.....

    @Override
    public void driverPickUpDetailsSuccess(PickupDropDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType) {
        if (pickUpDropTripsList == null)
            pickUpDropTripsList = new ArrayList<PickupDropDataModel>();
        else pickUpDropTripsList.clear();

        pickUpDropTripsList.addAll(travelDetailsModel.data);


//        for (int i = 0; i < pickUpDropTripsList.size(); i++) {
//            pickUpDropTripsList.get(i).isTripStarted = false;
//        }  //TODO: for temporory purpose  isTripStarted is Hardcoded


        //We will fetch Drop details now....
        getDropDetails();

    }//driverPickUpDetailsSuccess closes here,....

    @Override
    public void driverPickUpDetailsFailure(String errorMessage) {
        getDropDetails();
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        //We will fetch Drop details now....even if pickup was a failure.
        getDropDetails();
    }//driverPickUpDetailsFailure closes here.....

    @Override
    public void driverStartedTripSuccessfully(StartTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        Toast.makeText(this, responseModel.message, Toast.LENGTH_SHORT).show();
    }//driverStartedTripSuccessfully closes here.....

    @Override
    public void driverStartedTripFailure(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }//driverStartedTripFailure closes here.....


    @Override
    public void driverStoppedTripSuccessfully(StopTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        Toast.makeText(this, responseModel.message, Toast.LENGTH_SHORT).show();
    }//driverStoppedTripSuccessfully closes here.....

    @Override
    public void driverStoppedTripFailure(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }//driverStoppedTripFailure closes here.....

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.logoutBtn:
                Apputils.showConfirmationLogOut(this.getString(R.string.alert_title), this.getString(R.string.logout_confirmation_message), this);
                break;

            default:
                break;
        }//switch (view.getId()) closes here....
    }//public void onClick(View view) closes here....

    @Override
    public void onRefresh() {
        getPickUpDetails(false);
        pullToRefresh.setRefreshing(false);
    }

    ////////////.............API CALLS CLOSES ABOVE.............\\\\\\\\\\\\\\\\\
}
