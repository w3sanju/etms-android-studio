package com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler;

import android.content.Context;

import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;


@Deprecated
public class CheckOutClickHandler implements CheckOutResponseListener {


    //Medium Priority Non-UI variables goes below
    private GetAllTripsDataModel mAllTripsDataModel;
    private Context context;
    private CheckOutClickHandleInteractor mClickResponseHandler;
    private Integer pos;
    private double latitude,longitude;

    public CheckOutClickHandler(GetAllTripsDataModel allTripsDataModel, int position, Context mContext, CheckOutClickHandleInteractor clickResponseHandler,double latitude,double longitude) {

        mAllTripsDataModel = allTripsDataModel;
        context = mContext;
        this.mClickResponseHandler = clickResponseHandler;
        pos = position;
        this.latitude = latitude;
        this.longitude = longitude;
    }

//    @Override
//    public void onClick(View view) {
//        Apputils.showConfirmationStartStopTrip(context.getString(R.string.alert_title),context.getString(R.string.check_out_trip),context, CheckOutClickHandler.this);
//    }


   public void checkOutClick(){
       Map<String, String> startTripParams = new HashMap<>();
       startTripParams.put(AppConstants.SCHEDULED_DATE_KEY, mAllTripsDataModel.scheduled_date);
       startTripParams.put(AppConstants.VEHICLE_ID, mAllTripsDataModel.vehicle_id);
       startTripParams.put(AppConstants.SLOT_KEY, mAllTripsDataModel.slot);
       startTripParams.put(AppConstants.EMPLOYEE_ID, mAllTripsDataModel.empid);
       startTripParams.put(AppConstants.CHECK_OUT_LATITUDE_KEY, String.valueOf(latitude));
       startTripParams.put(AppConstants.CHECK_OUT_LONGITUDE_KEY,String.valueOf(longitude));

       CheckOutInteractor checkOutInteractor = new CheckOutInteractor(this, TRAVEL_TYPE_ENUM.DROP, context);
       checkOutInteractor.execute(startTripParams, pos);
   }

    @Override
    public void employeeCheckOutSuccessFully(CheckOutResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        //TODO: Remove BELOW LINE WHEN MERGING TO MASTER....
//        String response = "{\n" +
//                "    \"status\": true,\n" +
//                "    \"message\": \"Checked out successfully.\",\n" +
//                "    \"data\": null,\n" +
//                "    \"totalRecords\": 0\n" +
//                "}";
//        responseModel = new Gson().fromJson(response, CheckOutResponseModel.class);
        //Hardcoding closes above....




        if (responseModel.status)//if true...
            mClickResponseHandler.checkedOutSuccessfully(responseModel.message, responseModel, pos);
        else
            mClickResponseHandler.checkOutFailure(responseModel.message);
    }


    @Override
    public void employeeCheckOutFailure(String errorMessage) {
        mClickResponseHandler.checkOutFailure(errorMessage);
    }
}
