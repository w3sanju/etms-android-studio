package com.hgs.etms.Dashboard.Employee.GetAllTripsListing;

public interface GetAllLogoutTripsListener {

    void allLogoutEmployeeTripFetchedSuccessfully(GetAllTripsResponseModel employeeTripDetails, String message);

    void allLogoutEmployeeTripFetchFailure(String errorMessage);

}//GetAllLogoutTripsListener closes here....
