package com.hgs.etms.Dashboard.Driver.ViewDetails;

import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.GetTravelDetails.TravelDetailsModel;

public interface GetPickupDropDetailsListener {


    void pickupDropdetailsFetchedSuccessfully(TravelDetailsModel travelDetailsModel, int clickedPosition, TRAVEL_TYPE_ENUM travelType);

    void pickupDropDetailsFetchedFailure(String errorMessage);

}//GetPickupDropDetailsListener closes here....
