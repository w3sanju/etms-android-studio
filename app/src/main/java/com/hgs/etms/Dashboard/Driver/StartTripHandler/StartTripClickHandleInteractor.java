package com.hgs.etms.Dashboard.Driver.StartTripHandler;

public interface StartTripClickHandleInteractor {

    void tripStartedSuccessfully(String responseMessage, StartTripResponseModel startTripModel, int position);

    void tripStartedFailure(String errorMessage);
}
