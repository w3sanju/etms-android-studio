package com.hgs.etms.Dashboard.Driver.GetDriverDropDetails;

import com.hgs.etms.Dashboard.Driver.PickupDropDetailsModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;


public interface GetDriverDropDetailsListener {

    /**
     * @travelType: This will tell whether user is travelling for Drop or Pickup
     * **/
    void driverDropDetailsSuccess(PickupDropDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType);

    void driverDropDetailsFailure(String errorMessage);

}//GetDriverDropDetailsListener closes here....
