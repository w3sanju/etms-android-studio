package com.hgs.etms.Dashboard.Employee.GetAllTripsListing;

public interface GetAllLoginTripsListener {

    void allLoginEmployeeTripFetchedSuccessfully(GetAllTripsResponseModel employeeTripDetails, String message);

    void allLoginEmployeeTripFetchFailure(String errorMessage);

}//GetAllLoginTripsListener closes here....
