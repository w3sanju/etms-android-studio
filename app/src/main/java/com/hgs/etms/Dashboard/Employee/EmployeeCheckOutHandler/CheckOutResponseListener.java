package com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler;

import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;

public interface CheckOutResponseListener {

    void employeeCheckOutSuccessFully(CheckOutResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos);

    void employeeCheckOutFailure(String errorMessage);

}//StartTripResponseListener closes here.....
