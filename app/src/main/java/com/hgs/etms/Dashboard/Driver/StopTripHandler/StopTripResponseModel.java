package com.hgs.etms.Dashboard.Driver.StopTripHandler;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class StopTripResponseModel implements Parcelable {
    public boolean status;
    public String message;
    @Nullable
    public List<Object> data;
    private int totalRecords;


    protected StopTripResponseModel(Parcel in) {
        status = in.readByte() != 0x00;
        message = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<Object>();
            in.readList(data, Object.class.getClassLoader());
        } else {
            data = null;
        }
        totalRecords = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (status ? 0x01 : 0x00));
        dest.writeString(message);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        dest.writeInt(totalRecords);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StopTripResponseModel> CREATOR = new Parcelable.Creator<StopTripResponseModel>() {
        @Override
        public StopTripResponseModel createFromParcel(Parcel in) {
            return new StopTripResponseModel(in);
        }

        @Override
        public StopTripResponseModel[] newArray(int size) {
            return new StopTripResponseModel[size];
        }
    };
}//StopTripResponse model closes