package com.hgs.etms.Dashboard.Employee;

import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hgs.etms.Dashboard.Driver.DashboardActivity;
import com.hgs.etms.Dashboard.Employee.DashboardViewPager.CustomPagerComparator;
import com.hgs.etms.Dashboard.Employee.DashboardViewPager.CustomPagerModel;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllLoginTripsInteractor;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllLoginTripsListener;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllLogoutTripsInteractor;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllLogoutTripsListener;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsResponseModel;
import com.hgs.etms.Feedback.FeedbackDialogFragment;
import com.hgs.etms.Logout.Logout;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.Apputils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class EmployeeDashboardBaseActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener, GetAllLoginTripsListener, GetAllLogoutTripsListener, SwipeRefreshLayout.OnRefreshListener {

    //High priority variables goes below....
    private ViewPager viewPager;
    private TabAdapter adapter;
    private ImageButton logoutBtn;
    private TextView mLoggedInUsernameTxtV;
    private SwipeRefreshLayout pullToRefresh;
    private LinearLayout mActionBarContainer;
    private ProgressDialog progressDialog;
    List<GetAllTripsDataModel> allTripDataList;

    //Medium priority NON-UI variables goes below....
    private GetAllTripsResponseModel loginEmpTripDetails_AllDates;//This object has the details of all the Login trips, we need to filter it manually at run time & display the viewpager accordingly.
    private GetAllTripsResponseModel logoutEmpTripDetails_AllDates;//This object has the details of all the Logout trips, we need to filter it manually at run time & display the viewpager accordingly.
    private List<CustomPagerModel> listOfCustomPagerObjects;
    private CustomPagerModel todayPagerModel;
    private String mostPastDate;
//    private LocationTrack locationTrack;;
//    private double longitude ;
//    private double latitude ;


    //Least priority variables goes below....
    private final String TAG = "EmployeeDashboardBaseActivity".trim().substring(0,23);
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Step:1 >> initializing UI elements....
        initializations();
        mLoggedInUsernameTxtV.setText(PreferenceManager.getDefaultSharedPreferences(this).getString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, ""));


        //Step:2 >> Creating a List of dynamic data....
        //This data will be used to set Fragment dynamically....
        String currentDate = AppConstants.DD_MM_YYYY_DATEFORMAT.format(new Date());//TODO: UNCOMMENT FOR PROD PURPOSES....
    //   String currentDate = "26/10/2019";//TODO: COMMENT FOR PROD PURPOSES.....
        listOfCustomPagerObjects = new ArrayList<CustomPagerModel>();
        todayPagerModel = new CustomPagerModel();
        todayPagerModel.setDate(currentDate);
        todayPagerModel.setToday(true);
        listOfCustomPagerObjects.add(todayPagerModel);


        String pastDate = currentDate;
        for(int j=0 ; j<AppConstants.NO_OF_PAST_DAYS_IN_VIEWPAGER ; j++){
            pastDate = subtractOneDayFrom(pastDate);
            CustomPagerModel pastDateModel = new CustomPagerModel();
            pastDateModel.setToday(false);
            pastDateModel.setDate(pastDate);
            listOfCustomPagerObjects.add(pastDateModel);
        }

        String futureDate = currentDate;
        for(int i=0 ; i<AppConstants.NO_OF_FUTURE_DAYS_IN_VIEWPAGER ; i++) {
            futureDate = addOneDayTo(futureDate);
            CustomPagerModel futureDateModel = new CustomPagerModel();
            futureDateModel.setDate(futureDate);
            futureDateModel.setToday(false);
            listOfCustomPagerObjects.add(futureDateModel);
        }//for loop of future dates goes below....


        //Step:2.1 >> Sorting the above list w.r.t date....
        Collections.sort(listOfCustomPagerObjects, new CustomPagerComparator());




        getAllLoginTrips(true);

    }//onCreate closes here....

    void getAllLoginTrips(Boolean showProgress){

        if(showProgress) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Fetching Trips...");
            progressDialog.show();
        }

        //Since List is sorted now, so we will fetch the most past date, (i.e. the smallest dat) & call API to fetch all the data.....
        mostPastDate = listOfCustomPagerObjects.get(0).getDate();
        GetAllLoginTripsInteractor getAllLoginTrips = new GetAllLoginTripsInteractor(this, this);
        getAllLoginTrips.execute(mostPastDate);
    }//getAllLoginTrips closes here.....



    void initializations(){
        logoutBtn = findViewById(R.id.logoutBtn);
        viewPager = findViewById(R.id.pager);
        mLoggedInUsernameTxtV = findViewById(R.id.username);
        pullToRefresh = findViewById(R.id.pullToRefresh);

        mActionBarContainer = findViewById(R.id.layoutInclude);
        mActionBarContainer.bringToFront();
        pullToRefresh.setOnRefreshListener(this);

        //Changing the PullToRefresh color....
        pullToRefresh.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimaryDark));


        logoutBtn.setOnClickListener(this);
        viewPager.addOnPageChangeListener(this);


        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


    }//initializations closes here....



    /**
     * This function creates a new date, like passed date + 1 & returns as String.
     *
     * @param toDate ; We have to add 1 day in this date.
     * **/
    private String addOneDayTo(String toDate){
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(AppConstants.DD_MM_YYYY_DATEFORMAT.parse(toDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        String output = AppConstants.DD_MM_YYYY_DATEFORMAT.format(c.getTime());

        return output;
    }//addOneDayTo closes here....


    /**
     * This function creates a new date, like passed date - 1 & returns as String.
     *
     * @param fromDate ; We have to subtract 1 day from this date.
     * **/
    private String subtractOneDayFrom(String fromDate){
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(AppConstants.DD_MM_YYYY_DATEFORMAT.parse(fromDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        String output = AppConstants.DD_MM_YYYY_DATEFORMAT.format(c.getTime());

        return output;
    }//subtractOneDayFrom closes here....


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.side_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                new Logout(this).execute();
                return true;
            case R.id.home:
                Intent intentHome = new Intent(this, EmployeeDashboardBaseActivity.class);
                startActivity(intentHome);
                return true;
            case R.id.driver:
                Intent intentDriver = new Intent(this, DashboardActivity.class);
                startActivity(intentDriver);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }




    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(EmployeeDashboardBaseActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.logoutBtn:
               // new Logout(this).execute();
                Log.e("CLICKED","TRUE");
                Apputils.showConfirmationLogOut(this.getString(R.string.alert_title), this.getString(R.string.logout_confirmation_message), this);
                break;

            default:
                break;
        }//switch (view.getId()) closes here....
    }//public void onClick(View view) closes here....


   private void displayFeedbackAlert(GetAllTripsDataModel getAllTripsDataModel){

        Bundle detailsExtras = new Bundle();
        detailsExtras.putParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY,getAllTripsDataModel );

        String backstackTAG = "FeedbackDialog";
        FeedbackDialogFragment detailsFragDialog = new FeedbackDialogFragment();
        detailsFragDialog.setArguments(detailsExtras);
        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();//getSupportFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(null);
        detailsFragDialog.show(fragmentTransaction, backstackTAG);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//        Log.d(TAG, "Pawan in onPageScrolled, position = "+position+", positionOffset = "+positionOffset+", positionOffsetPixels = "+positionOffsetPixels);
    }//onPageScrolled closes here....

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "In onPageSelected, position = "+position);
    }//onPageSelected closes here.....

    @Override
    public void onPageScrollStateChanged(int state) {
//        Log.d(TAG, "Pawan in onPageScrollStateChanged, position = "+state);
    }//onPageScrollStateChanged closes here....


    void setupViewPagerAdapter(){
        //Step:3 >> Setup Adapter for ViewPager....
        adapter = new TabAdapter(getSupportFragmentManager());
        allTripDataList = new ArrayList<>();
        for(int i=0 ; i<listOfCustomPagerObjects.size() ; i++){

            //Filtering the Original All List with the date in the loop....
            //i.e. all list has multiple date, but we need date which is in the loop.
            //therefore applying filter.

            String ithDate = Apputils.convertDate(AppConstants.DD_MM_YYYY_DATEFORMAT, AppConstants.YYYY_MM_DD_DATEFORMAT, listOfCustomPagerObjects.get(i).date);
            List<GetAllTripsDataModel> filteredLoginTripList = new ArrayList<>(), filteredLogoutTripList = new ArrayList<>();


            //Filtering the Login trip list....
            if(loginEmpTripDetails_AllDates != null) {

                allTripDataList.addAll(loginEmpTripDetails_AllDates.data);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    filteredLoginTripList = this.loginEmpTripDetails_AllDates.data.stream().filter(tripObject -> tripObject.scheduled_date.equals(ithDate)).collect(Collectors.toList());
                }//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) closes here....
                else {
                    //We will filter wil manual loop....
                    for (GetAllTripsDataModel dataModel : loginEmpTripDetails_AllDates.data) {
                        if (dataModel.scheduled_date.equals(ithDate)) {
                            filteredLoginTripList.add(dataModel);

                        }//if(dataModel.scheduled_date.equals(ithDate)) closes here....
                        else {
                            Log.d(TAG, "Dates not matching, therefore ignoring to add in the list");
                        }//else closes here.....
                    }//for(GetAllTripsDataModel dataModel : employeeTripDetails_AllDates.data) closes here....
                }//else manual loop closes here....
            }//if(loginEmpTripDetails_AllDates != null) closes here....
            else{
                Log.w(TAG, "login trip details are null.");
            }//else closes here.....



            //Filtering the Logout trip list.....
            if(logoutEmpTripDetails_AllDates != null) {

                allTripDataList.addAll(logoutEmpTripDetails_AllDates.data);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    filteredLogoutTripList = this.logoutEmpTripDetails_AllDates.data.stream().filter(tripObject -> tripObject.scheduled_date.equals(ithDate)).collect(Collectors.toList());
                }//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) closes here....
                else {
                    //We will filter wil manual loop....
                    for (GetAllTripsDataModel dataModel : logoutEmpTripDetails_AllDates.data) {
                        if (dataModel.scheduled_date.equals(ithDate)) {
                            filteredLogoutTripList.add(dataModel);
                           // allTripDataList.add(dataModel);
                        }//if(dataModel.scheduled_date.equals(ithDate)) closes here....
                        else {
                            Log.d(TAG, "Dates not matching, therefore ignoring to add in the list");
                        }//else closes here.....
                    }//for(GetAllTripsDataModel dataModel : employeeTripDetails_AllDates.data) closes here....
                }//else manual loop closes here....
            }//if(logoutEmpTripDetails_AllDates != null) closes here....
            else{
                Log.w(TAG, "logout trip details are null.");
            }//else closes here.....





            Bundle employeeFragBundle = new Bundle();
            employeeFragBundle.putParcelable(AppConstants.EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY, listOfCustomPagerObjects.get(i));
            employeeFragBundle.putParcelableArrayList(AppConstants.EMPLOYEE_LOGIN_TRIPS_KEY, (ArrayList<GetAllTripsDataModel>) filteredLoginTripList);
            employeeFragBundle.putParcelableArrayList(AppConstants.EMPLOYEE_LOGOUT_TRIPS_KEY, (ArrayList<GetAllTripsDataModel>) filteredLogoutTripList);




            EmployeeDashboardFragment employeeDashboardFrag = new EmployeeDashboardFragment();
            employeeDashboardFrag.setArguments(employeeFragBundle);

            adapter.addFragment(employeeDashboardFrag, "");


        }//for(int i=0 ; i<listOfDates.size() ; i++) closes here....

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(listOfCustomPagerObjects.indexOf(todayPagerModel));


        if(allTripDataList!=null){
            for(int i=0;i<allTripDataList.size();i++){
                if(allTripDataList.get(i).is_checked_in&&allTripDataList.get(i).is_checked_out&&!allTripDataList.get(i).is_feedback_submited){
                    displayFeedbackAlert(allTripDataList.get(i));
                    break;
                }else {
                    Log.w(TAG,"Trip not completed or feedback submitted");
                }
            }
        }




    }//setupViewPagerAdapter closes here....





    //////////..........METHODS FOR API CALLS GOES BELOW...........\\\\\\\\\\\\
    @Override
    public void allLoginEmployeeTripFetchedSuccessfully(GetAllTripsResponseModel employeeTripDetails, String message) {
        this.loginEmpTripDetails_AllDates = employeeTripDetails;

        GetAllLogoutTripsInteractor getAllLogoutTrips = new GetAllLogoutTripsInteractor(this, this);
        getAllLogoutTrips.execute(mostPastDate);
    }//allEmployeeTripFetchedSuccessfully closes here.....

    @Override
    public void allLoginEmployeeTripFetchFailure(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        GetAllLogoutTripsInteractor getAllLogoutTrips = new GetAllLogoutTripsInteractor(this, this);
        getAllLogoutTrips.execute(mostPastDate);

    }//allEmployeeTripFetchFailure closes here.....

    @Override
    public void allLogoutEmployeeTripFetchedSuccessfully(GetAllTripsResponseModel employeeTripDetails, String message) {
        if(progressDialog!=null)
            progressDialog.dismiss();
        this.logoutEmpTripDetails_AllDates = employeeTripDetails;
        setupViewPagerAdapter();
    }//allLogoutEmployeeTripFetchedSuccessfully closes here....

    @Override
    public void allLogoutEmployeeTripFetchFailure(String errorMessage) {
        if(progressDialog!=null)
            progressDialog.dismiss();
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        setupViewPagerAdapter();
    }//allLogoutEmployeeTripFetchFailure closes here....

    @Override
    public void onRefresh() {
        getAllLoginTrips(false);
        pullToRefresh.setRefreshing(false);
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel(this.getString(R.string.permission_mandatory),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            else
                                                Log.w(TAG, "Version SDK Int < VERSIOn_CODES.M");
                                        }//onClick closes here....
                                    });
                            return;
                        }//if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) closes here....
                        else
                            Log.w(TAG, "shouldShowRequestPermissionRationale(permissionsRejected.get(0)) is false");
                    }//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) closes here....
                    else
                        Log.w(TAG, "Version SDK Int < VERSIOn_CODES.M");
                }//if (permissionsRejected.size() > 0) closes here.....
                else
                    Log.w(TAG, "permissionRejected Size is <= 0");
                break;

            default:
                Log.w(TAG, "Unhandled request Code : " + requestCode);
                break;

        }//switch (requestCode) closes here....

    }//onRequestPermissionsResult closes here....

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }



}//EmployeeDashboardBaseActivity closes here.....
