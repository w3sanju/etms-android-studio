package com.hgs.etms.Dashboard.Employee.GetAllTripsListing;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class GetAllTripsResponseModel implements Parcelable {

    public Boolean status;
    public String message;
    public List<GetAllTripsDataModel> data;
    public Integer totalRecords;

    protected GetAllTripsResponseModel(Parcel in) {
        byte statusVal = in.readByte();
        status = statusVal == 0x02 ? null : statusVal != 0x00;
        message = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<GetAllTripsDataModel>();
            in.readList(data, GetAllTripsDataModel.class.getClassLoader());
        } else {
            data = null;
        }
        totalRecords = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (status == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (status ? 0x01 : 0x00));
        }
        dest.writeString(message);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        if (totalRecords == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(totalRecords);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GetAllTripsResponseModel> CREATOR = new Parcelable.Creator<GetAllTripsResponseModel>() {
        @Override
        public GetAllTripsResponseModel createFromParcel(Parcel in) {
            return new GetAllTripsResponseModel(in);
        }

        @Override
        public GetAllTripsResponseModel[] newArray(int size) {
            return new GetAllTripsResponseModel[size];
        }
    };



}//GetAllTripsResponseModel closes here....
