package com.hgs.etms.Dashboard.Driver;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripClickHandleInteractor;
import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripClickHandler;
import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripResponseModel;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripClickHandleInteractor;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripClickHandler;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripResponseModel;
import com.hgs.etms.Dashboard.Driver.ViewDetails.GetPickDropDetailsClickHandler;
import com.hgs.etms.Dashboard.Driver.ViewDetails.GetPickupDropDetailsListener;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.GetTravelDetails.TravelDetailsDialogFragment;
import com.hgs.etms.GetTravelDetails.TravelDetailsModel;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;


public class PickupDropAdapter extends RecyclerView.Adapter implements StartTripClickHandleInteractor, StopTripClickHandleInteractor, GetPickupDropDetailsListener {


    //High priority variables goes below.....
    private List<PickupDropDataModel> mPickupDropTripsList;
    private final Context mContext;
    private ArrayList<PickupDropSlotModel> mPickupDropSlotModels ;

    //Medium Priority goes here...


    //Least priority variables goes below....
    private final String TAG = "PickupDropAdapter";
    private final int PICKUP_OR_LOGIN = 0, DROP_OR_LOGOUT = 1;




    public PickupDropAdapter(Context context, List<PickupDropDataModel> pickUpDropTripsList) {
        this.mContext = context;
        this.mPickupDropTripsList = pickUpDropTripsList;
        Collections.sort(this.mPickupDropTripsList, PickupDropDataModel.FruitNameComparator);

//        String slotStarted =  PreferenceManager.getDefaultSharedPreferences(mContext).getString(AppConstants.SF_STARTED_SLOTS_AND_DATE, "");
//        Log.d("this is actual list",slotStarted);
//
//        Type type = new TypeToken<List<PickupDropSlotModel>>() {}.getType();
//        mPickupDropSlotModels = new Gson().fromJson(slotStarted, type);
//
//        Log.d("this is actual  object",""+mPickupDropSlotModels);
//
//        if(mPickupDropSlotModels!=null){
//            Log.d("this is object size:",""+mPickupDropSlotModels.size());
//        }

    }//PickupDropAdapter constructor closes here.....

    @Override
    public int getItemViewType(int position) {

        switch (PICKUP_DROP_TYPE_ENUM.valueOf((mPickupDropTripsList.get(position).slot_type.trim()))) {

            case LOGIN:
                return PICKUP_DROP_TYPE_ENUM.LOGIN.getValue();

            case LOGOUT:
                return PICKUP_DROP_TYPE_ENUM.LOGOUT.getValue();

            default:
                return PICKUP_DROP_TYPE_ENUM.LOGIN.getValue();

        }//switch (TRAVEL_TYPE_ENUM.valueOf((mPickupDropTripsList.get(position).slot_type.trim())) closes here.....
    }//getItemViewType closes here.....


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_login, parent, false);


        return new PickDropViewHolder(view, viewType);
    }//onCreateViewHolder closes here.....


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PickDropViewHolder pickupDropHolder = (PickDropViewHolder) holder;


        RecyclerView.LayoutParams pickupDropContainerParams;
        if (position == 0) {
            //We will add extra top margin, if 0th position....
            pickupDropContainerParams = (RecyclerView.LayoutParams) pickupDropHolder.mPickupParentContainer.getLayoutParams();
            int topMargin = Math.round(mContext.getResources().getDimension(R.dimen.login_hgsLogoTopMargin)) + 100;
            pickupDropContainerParams.setMargins(0, topMargin, 0, 0);
        }//if(position == 0) closes here.....


        TRAVEL_TYPE_ENUM traveltype = TRAVEL_TYPE_ENUM.PICKUP;//Bcoz depending on this we have to fetch travel details on show on the Alert Dialog.
        switch (holder.getItemViewType()) {

            case PICKUP_OR_LOGIN:
                //Show data for Pickup....

                pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.todaysPickupSpelling));
                pickupDropHolder.mPickupDropImageV.setImageResource(R.drawable.ic_car_login);
                pickupDropHolder.mPickupDropImageV.setBackgroundResource(R.drawable.pickup_bg_patch);
                traveltype = TRAVEL_TYPE_ENUM.PICKUP;
                break;

            case DROP_OR_LOGOUT:
                //Show data for Drop....

                pickupDropHolder.mPickUpDropheadingTxtV.setText(mContext.getString(R.string.todaysDropSpelling));
                pickupDropHolder.mPickupDropImageV.setImageResource(R.drawable.ic_car_logout);
                pickupDropHolder.mPickupDropImageV.setBackgroundResource(R.drawable.drop_bg_patch);
                traveltype = TRAVEL_TYPE_ENUM.DROP;
                break;
        }//switch (holder.getItemViewType()) closes here....


//        if(mPickupDropSlotModels!=null) {
//            for (int i = 0; i < mPickupDropSlotModels.size(); i++) {
//                if (mPickupDropTripsList.get(position).slot.equals(mPickupDropSlotModels.get(i).slot) && mPickupDropTripsList.get(position).scheduled_date.equals(mPickupDropSlotModels.get(i).scheduleDate)
//                        && mPickupDropTripsList.get(position).slot_type.equals(mPickupDropSlotModels.get(i).slot_type)) {
//                    mPickupDropTripsList.get(position).isTripStarted = true;
//
//                    break;
//                } else {
//                    mPickupDropTripsList.get(position).isTripStarted = false;
//                }
//            }
//        }


        //Start Trip, Stop trip goes below.....
        pickupDropHolder.mPickUpDropTimeDateTxtV.setText(mPickupDropTripsList.get(position).slot + " | " + mPickupDropTripsList.get(position).scheduled_date);

          Boolean isTripStarted = mPickupDropTripsList.get(position).isTripStarted;
          Boolean isTripStopped = mPickupDropTripsList.get(position).isTripStoped;

        if (isTripStarted != null&&isTripStopped!= null) {
            if (isTripStarted&&!isTripStopped)//1 0
            {
                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.stopTripSpelling));

                StopTripClickHandler stopTripHandler = new StopTripClickHandler(this.mPickupDropTripsList.get(position), position, mContext, this);
                pickupDropHolder.mBtnStartStopTrip.setOnClickListener(stopTripHandler);

            } else if(!isTripStarted&&!isTripStopped)//0 0
            {
                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.startTripSpelling));
                StartTripClickHandler startTripHandler = new StartTripClickHandler(this.mPickupDropTripsList.get(position), position, mContext, this);
                pickupDropHolder.mBtnStartStopTrip.setOnClickListener(startTripHandler);
            }else if(!isTripStarted&&isTripStopped) // 0 1
            {
                Log.w(TAG, "Trip should start first for stop");
            }else if(isTripStarted&&isTripStopped) // 1 1
            {
                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.tripCompleted));
                pickupDropHolder.mBtnStartStopTrip.setEnabled(false);
                pickupDropHolder.mBtnStartStopTrip.setBackgroundColor(mContext.getResources().getColor(R.color.transparentGrey));
            }else {
                pickupDropHolder.mBtnStartStopTrip.setText(mContext.getString(R.string.startTripSpelling));
                StartTripClickHandler startTripHandler = new StartTripClickHandler(this.mPickupDropTripsList.get(position), position, mContext, this);
                pickupDropHolder.mBtnStartStopTrip.setOnClickListener(startTripHandler);

            }
        }//if (mPickupDropTripsList.get(position).isTripStarted != null&& isTripStopped != null) closes here....
        else {
            Log.w(TAG, "mPickupDropTripsList.get(position).isTripStarted  is null.");
            Log.w(TAG, "mPickupDropTripsList.get(position).isTripStopped  is null.");
        }



        //View Details goes below.....


        pickupDropHolder.mViewDetailsTxtV.setText(mContext.getString(R.string.employeeBoardingSpelling));
        pickupDropHolder.mViewDetailsTxtV.setOnClickListener(new GetPickDropDetailsClickHandler(pickupDropHolder.mViewDetailsTxtV.getContext()
                , this, this.mPickupDropTripsList.get(position), position, traveltype));
    }//onBindViewHolder closes here.....

    @Override
    public int getItemCount() {
        if (mPickupDropTripsList == null)
            return 0;
        else
            return mPickupDropTripsList.size();
    }//getItemCount closes here.....


    @Override
    public void tripStartedSuccessfully(String responseMessage, StartTripResponseModel startTripModel, int position) {

        if (startTripModel.status) {

            mPickupDropTripsList.get(position).isTripStarted = true;
            mPickupDropTripsList.get(position).isTripStoped = false;
            notifyItemChanged(position);

//            PickupDropSlotModel dropSlotModel = new PickupDropSlotModel();
//            dropSlotModel.setScheduleDate(mPickupDropTripsList.get(position).scheduled_date);
//            dropSlotModel.setSlot(mPickupDropTripsList.get(position).slot);
//            dropSlotModel.setSlot_type(mPickupDropTripsList.get(position).slot_type);
//
//            if (mPickupDropSlotModels == null)
//                mPickupDropSlotModels = new ArrayList<PickupDropSlotModel>();
//
//            mPickupDropSlotModels.add(dropSlotModel);
//
//            Gson gson = new Gson();
//            String jsonString = gson.toJson(mPickupDropSlotModels);
//
//            PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(AppConstants.SF_STARTED_SLOTS_AND_DATE, jsonString.trim()).apply();

        } else {
            Log.w(TAG, "" + startTripModel.status);
        }
    }

    @Override
    public void tripStartedFailure(String errorMessage) {
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void tripStoppedSuccessfully(String responseMessage, StopTripResponseModel stopTripModel, int position) {
        if (stopTripModel.status) {
//            if(mPickupDropSlotModels!=null) {
//                for (int i = 0; i < mPickupDropSlotModels.size(); i++) {
//                    if (mPickupDropTripsList.get(position).slot.equals(mPickupDropSlotModels.get(i).slot) && mPickupDropTripsList.get(position).scheduled_date.equals(mPickupDropSlotModels.get(i).scheduleDate)
//                            && mPickupDropTripsList.get(position).slot_type.equals(mPickupDropSlotModels.get(i).slot_type)) {
//                        mPickupDropSlotModels.remove(i);
//                        break;
//                    }
//                }
//
//            }//if(mPickupDropSlotModels!=null) closes here....
//            else
//                Log.w(TAG, "mPickupDropSlotModels is null.");
//
//
//            Gson gson = new Gson();
//            String jsonString = gson.toJson(mPickupDropSlotModels);
//
//            PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(AppConstants.SF_STARTED_SLOTS_AND_DATE, jsonString.trim()).apply();
//            mPickupDropTripsList.get(position).isTripStarted = false;

            mPickupDropTripsList.get(position).isTripStarted = true;
            mPickupDropTripsList.get(position).isTripStoped = true;
            notifyItemChanged(position);

        } else {
            Log.w(TAG, "" + stopTripModel.status);
        }
    }//tripStoppedSuccessfully closes here....


    @Override
    public void tripStoppedFailure(String errorMessage) {
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
    }


    /**
     * When user clicks on View DetAils, the API is called & we get respnse here....
     **/
    @Override
    public void pickupDropdetailsFetchedSuccessfully(TravelDetailsModel travelDetailsModel, int clickedPosition, TRAVEL_TYPE_ENUM traveltype) {
        //Toast.makeText(mContext, travelDetailsModel.message, Toast.LENGTH_SHORT).show();


        String userType =   PreferenceManager.getDefaultSharedPreferences(mContext).getString(AppConstants.SF_LOGGED_IN_USER_TYPE, "");

        Bundle detailsExtras = new Bundle();
        detailsExtras.putParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY, travelDetailsModel);
        detailsExtras.putSerializable(AppConstants.TRAVEL_TYPE_KEY, traveltype);
        detailsExtras.putString(AppConstants.SF_LOGGED_IN_USER_TYPE, userType);



        String backstackTAG = "PickUpDropDetailsAlertDialog";
        TravelDetailsDialogFragment detailsFragDialog = new TravelDetailsDialogFragment();
        detailsFragDialog.setArguments(detailsExtras);
        FragmentTransaction fragmentTransaction = ((Activity) mContext).getFragmentManager().beginTransaction();//getSupportFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack(backstackTAG);
        detailsFragDialog.show(fragmentTransaction, backstackTAG);

    }//pickupDropdetailsFetchedSuccessfully closes here.....

    /**
     * When user clicks on View  details, & due to some error we are not able to fetch details, then control comes here..
     **/
    @Override
    public void pickupDropDetailsFetchedFailure(String errorMessage) {
        //Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
        Log.w(TAG,errorMessage);
    }//pickupDropDetailsFetchedFailure closes here.....


    private class PickDropViewHolder extends RecyclerView.ViewHolder {

        //High priority UI variables goes below.....
        private TextView mPickUpDropheadingTxtV, mPickUpDropTimeDateTxtV, mViewDetailsTxtV;
        private Button mBtnStartStopTrip;
        private ImageView mPickupDropImageV;
        private LinearLayout mPickupParentContainer;


        public PickDropViewHolder(View view, int viewType) {
            super(view);

            mPickUpDropheadingTxtV = view.findViewById(R.id.tvPickupDropHeading);
            mPickUpDropTimeDateTxtV = view.findViewById(R.id.tvPickupDropTimeDate);
            mViewDetailsTxtV = view.findViewById(R.id.tvViewDetails);
            mBtnStartStopTrip = view.findViewById(R.id.btnStartStopPickup);
            mPickupDropImageV = view.findViewById(R.id.imgPickupDropV);
            mPickupParentContainer = view.findViewById(R.id.pickupContainer);
        }//PickDropViewHolder constructor closes here.....
    }//PickDropViewHolder class closes here.....

}//PickupDropAdapter class closes here....
