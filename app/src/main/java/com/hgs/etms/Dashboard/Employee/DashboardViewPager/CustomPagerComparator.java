package com.hgs.etms.Dashboard.Employee.DashboardViewPager;

import com.hgs.etms.utils.AppConstants;

import java.text.ParseException;
import java.util.Comparator;

public class CustomPagerComparator implements Comparator<CustomPagerModel> {

    @Override
    public int compare(CustomPagerModel mod1, CustomPagerModel mod2) {
        try{
            return AppConstants.DD_MM_YYYY_DATEFORMAT.parse(mod1.date).compareTo(AppConstants.DD_MM_YYYY_DATEFORMAT.parse(mod2.date));
        }
        catch(ParseException pse){
            pse.printStackTrace();
            return 0;//This ill tell that both are equal.
        }//pse closes here....
    }
}//CustomPagerComparator closes here....
