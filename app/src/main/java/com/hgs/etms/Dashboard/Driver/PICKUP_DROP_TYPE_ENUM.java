package com.hgs.etms.Dashboard.Driver;

public enum PICKUP_DROP_TYPE_ENUM {
    LOGIN(0),//Login is like Pickup....
    LOGOUT(1);//Logout is like Drop....

    private final int value;

    PICKUP_DROP_TYPE_ENUM(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }

}//PickupDropTypeENUM closes here.....
