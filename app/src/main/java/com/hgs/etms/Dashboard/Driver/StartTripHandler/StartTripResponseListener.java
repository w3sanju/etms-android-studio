package com.hgs.etms.Dashboard.Driver.StartTripHandler;

import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;

public interface StartTripResponseListener {

    void driverStartedTripSuccessfully(StartTripResponseModel responseModel, TRAVEL_TYPE_ENUM travelType,Integer pos);

    void driverStartedTripFailure(String errorMessage);

}//StartTripResponseListener closes here.....
