package com.hgs.etms.utils;

import java.text.SimpleDateFormat;

public class AppConstants {


    ///////.......CONSTANTS FOR SHAREDPREFS GOES BELOW..........\\\\\\\\\\
    public static final String SF_LOGGED_IN_USER_TYPE = "LOGGED_IN_USER_TYPE";
    public static final String SF_LOGGED_IN_USER_EMP_ID = "LOGGED_IN_EMPLOYEE_ID";
    public static final String SF_STARTED_SLOTS_AND_DATE = "STARTED_SLOTS_AND_DATE";
    public static final String SF_LOGGED_IN_USER_EMP_FNAME = "LOGGED_IN_EMPLOYEE_FNAME";
    public static final String SF_DEVICE_TOKEN_FCM = "SF_DEVICE_TOKEN_FCM";



    /////////........CONSTANTS FOR API CALL GOES BELOW............\\\\\\\\\\\\\
    public static final String SLOT_KEY = "Slot";
    public static final String SCHEDULED_DATE_KEY = "ScheduledDate";
    public static final String DRIVER_NAME_KEY = "DriverName";
    public static final String SLOT_TYPE_KEY = "slotType";
    public static final String SLOT_TYPE_CAMELCASE_KEY = "SlotType";
    public static final String SLOT_TYPE_LOGIN_VALUE = "LOGIN";
    public static final String SLOT_TYPE_LOGOUT_VALUE = "LOGOUT";
    public static final String VEHICLE_ID = "VehicleId";
    public static final String EMPLOYEE_ID = "EmployeeId";
    public static final String EMPLOYEE_ID_KEY = "Employed";
    public static final String CHECK_OUT_LATITUDE_KEY = "CheckoutLatitude";
    public static final String CHECK_OUT_LONGITUDE_KEY = "CheckoutLongitude";
    public static final String CHECK_IN_LATITUDE_KEY = "CheckinLatitude";
    public static final String CHECK_IN_LONGITUDE_KEY = "CheckinLongitude";
    public static final String DEVICE_TOKEN_PARAM = "device_token";
    public static final String DEVICE_TYPE_PARAM = "device_type";
    public static final String DEVICE_TYPE_VALUE = "A";
    public static final String RATING_KEY = "Rating";
    public static final String FEEDBACK_KEY = "Feedback";



    ////////.......CONSTANTS FOR DATE FORMAT GOES BELOW...........\\\\\\\\\\\
    public static final SimpleDateFormat YYYY_MM_DD_DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat DD_MM_YYYY_DATEFORMAT = new SimpleDateFormat("dd/MM/yyyy");



    ////////.........PARCELABLE KEYS GOES BELOW.............\\\\\\\\\\\\\\
    public static final String TRAVEL_DETAILS_OBJECT_KEY = "TRAVEL_DETAILS_OBJECT";
    public static final String TRAVEL_TYPE_KEY = "TRAVEL_TYPE";
    public static final String EMPLOYEE_FRAGMENT_CUSTOM_OBJECT_KEY = "EMPLOYEE_FRAGMENT_CUSTOM_OBJECT";
    public static final String EMPLOYEE_LOGIN_TRIPS_KEY = "EMPLOYEE_LOGIN_TRIPS";
    public static final String EMPLOYEE_LOGOUT_TRIPS_KEY = "EMPLOYEE_LOGOUT_TRIPS";
    public static final String TIMESLOT_INTENT_KEY = "TIMESLOT";
    public static final String SCHEDULED_DATE_INTENT_KEY = "SCHEDULED_DATE";
    public static final String CLICKED_POSITION_INTENT_KEY = "CLICKED_POSITION_INTENT";
    public static final String EMPLOYEE_CHECK_IN_BROADCAST = "com.hgs.etms/EmployeeCheckin";
    public static final String EMPLOYEE_CHECKOUT_BROADCAST = "com.hgs.etms/EmployeeCheckOut";
    public static final String EMPLOYEE_LATITUDE= "EMP_LATITUDE";
    public static final String EMPLOYEE_LONGITUDE = "EMP_LONGITUDE";



    ///////////........CONSTANTS FOR UI GOES BELOW.............\\\\\\\\\\\\\\\
    public static final int NO_OF_FUTURE_DAYS_IN_VIEWPAGER = 2;
    public static final int NO_OF_PAST_DAYS_IN_VIEWPAGER = 1;


    //////////...........CONSTANTS FOR VOLLEY GOES BELOW..............\\\\\\\\\\\\\
    public static final int VOLLEY_TIMEOUT = 6000;
    public static final int VOLLEY_MAX_RETRIES = 2;
    public static final float VOLLEY_BACKOFF_MULT = 2f;



}//AppConstants closes here.....
