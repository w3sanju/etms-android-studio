package com.hgs.etms.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import com.hgs.etms.Dashboard.Driver.StartTripHandler.StartTripClickHandler;
import com.hgs.etms.Dashboard.Driver.StopTripHandler.StopTripClickHandler;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutClickHandler;
import com.hgs.etms.Logout.Logout;
import com.hgs.etms.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;

public  class Apputils {

   private Context ctx;
    public static String convertDate(DateFormat fromDateFormat, DateFormat toDateFormat, String dateToConvert) {

        if (dateToConvert == null)
            return "";
        else if (dateToConvert.isEmpty())
            return "";
        else {
            //Converting date now....
            try {
                Date newDate = fromDateFormat.parse(dateToConvert);
                dateToConvert = toDateFormat.format(newDate);
                return dateToConvert;
            }//try closes here....
            catch (ParseException pse) {
                pse.printStackTrace();
                return "";
            }//catch closes here....
        }//else closes here.....

    }//convertDate closes here....


   public static void showConfirmationLogOut(String title, String message, final Activity mContext){

       new AlertDialog.Builder(mContext)
               .setTitle(title)
               .setMessage(message)
               .setIcon(android.R.drawable.ic_dialog_alert)
               .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {

                   public void onClick(DialogInterface dialog, int whichButton) {
                       new Logout(mContext).execute();
                   }})
               .setNegativeButton(mContext.getString(R.string.no), null).show();


   }

    public static void showConfirmationStartStopTrip(String title, String message, final Context mContext, final Object handler){

        final String TAG = "showConfirmationStartStopTrip".toString().trim().substring(0,23);
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(handler instanceof StartTripClickHandler){
                            ((StartTripClickHandler) handler).startTripClick();
                        }//if(handler instanceof StartTripClickHandler) closes here.....
                        else if(handler instanceof StopTripClickHandler){
                            ((StopTripClickHandler) handler).stopTripClick();
                        } else if(handler instanceof CheckOutClickHandler){
                            ((CheckOutClickHandler) handler).checkOutClick();
                        }
                        else{
                            Log.w(TAG, "Unhandled instance of Handler.");
                        }
                    }})
                .setNegativeButton(mContext.getString(R.string.no), null).show();


    }//showConfirmationStartStopTrip closes here.....



}//Apputils closes here.....
