package com.hgs.etms.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.hgs.etms.MainActivity;
import com.hgs.etms.R;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

public class ETMSMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {



    //Least priority variables goes below....
    private final String TAG = "ETMSMessagingService";


    public ETMSMessagingService() {

    }//ETMSMessagingService constructor closes here....


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getNotification());

            RemoteMessage.Notification notification = remoteMessage.getNotification();
//            Map<String, String> data = remoteMessage.getData();

            sendNotification(notification, null);
        }//if (remoteMessage.getNotification() != null) closes here....
        else{
            Log.w(TAG, "remoteMessage.getNotification() is null.");
        }//else closes here.....

    }//onMessageReceived closes here....



    /**
     * Create and show a custom notification containing the received FCM message.
     *
     * @param notification FCM notification payload received.
     * @param data FCM data payload received.
     */
    private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data) {

        String channelID = "Default";
        String channelName = "Default";
        String channelDescription = "All the default Notifications are in this category.";
        int notificationImportance = NotificationManager.IMPORTANCE_HIGH;
        Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        String notificationMessage = data.get("body");
//        String notificationTitle = data.get("title");

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelID)
                .setContentTitle(notification.getTitle())
                .setContentText(notification.getBody())
                .setAutoCancel(true)
                .setSound(notificationSoundUri)
                .setContentIntent(pendingIntent)
                .setContentInfo(notification.getTitle())
                .setLargeIcon(icon)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setLights(getResources().getColor(R.color.colorPrimary), 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.mipmap.ic_launcher);

//        try {
//            String picture_url = data.get("icon");
//            if (picture_url != null && !"".equals(picture_url)) {
//                URL url = new URL(picture_url);
//                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                notificationBuilder.setStyle(
//                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
//                );
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Notification Channel is required for Android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(
                    channelID, channelName, notificationImportance
            );
            channel.setDescription(channelDescription);
            channel.setShowBadge(false);
            channel.setSound(notificationSoundUri, null);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(getResources().getColor(R.color.colorPrimary));
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(NotificationID.getID(), notificationBuilder.build());

    }//sendNotification closes here.....

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token);
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(AppConstants.DEVICE_TOKEN_PARAM, token).apply();
    }//onNewToken closes here....





}//ETMSMessagingService class closes here.....


class NotificationID {
    private static final AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }
}//NotificationID closes here....