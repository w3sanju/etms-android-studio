package com.hgs.etms.utils;


import android.os.Build;
import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class is used for Encryption & Decryption of the String used in the App.
 * **/
public class AESCrypt {

    //High priority variables goes below.....
    private String aesSecretkey;


    //Least priority variables goes below.....
    private final String TAG = "AESCrypt";


    public AESCrypt(String aesSecretKey){
        this.aesSecretkey = aesSecretKey;
    }//constructor closes here....


    public String encrypt(String value) {
        try {

            byte[] key = aesSecretkey.getBytes("UTF-8");
            byte[] ivs = aesSecretkey.getBytes("UTF-8");;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                // only for gingerbread and newer versions
                key = aesSecretkey.getBytes(StandardCharsets.UTF_8);
                ivs = aesSecretkey.getBytes(StandardCharsets.UTF_8);
            }
            else{
                key = aesSecretkey.getBytes("UTF-8");
                ivs = aesSecretkey.getBytes("UTF-8");
            }

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(ivs);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, paramSpec);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                return Base64.encodeToString(cipher.doFinal(value.getBytes(StandardCharsets.UTF_8)), Base64.DEFAULT);
            else
                return Base64.encodeToString(cipher.doFinal(value.getBytes("UTF-8")), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }//encrypt closes here.....


}//AESCrypt closes here....
