package com.hgs.etms.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.hgs.etms.MainActivity;
import com.hgs.etms.R;

import androidx.core.app.NotificationCompat;

public class DisplayLocalNotification {

    //High priority variables goes below....
    private static final DisplayLocalNotification ourInstance = new DisplayLocalNotification();
    public static DisplayLocalNotification getInstance() {
        return ourInstance;
    }

    //Least priority variables goes below....
    private final String TAG = "DisplayLocalNotification".toString().substring(0,23);


    //Private constructor goes below....
    private DisplayLocalNotification() {}//private constructor closes here....


    public void displayNotification(String title, String message, Resources mResources, Context mContext){
        String channelID = "Default";
        String channelName = "Default";
        String channelDescription = "All the default Notifications are in this category.";
        int notificationImportance = NotificationManager.IMPORTANCE_HIGH;
        Uri notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        String notificationMessage = data.get("body");
//        String notificationTitle = data.get("title");

        Bitmap icon = BitmapFactory.decodeResource(mResources, R.mipmap.ic_launcher);
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext, channelID)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(notificationSoundUri)
                .setContentIntent(pendingIntent)
                .setContentInfo(title)
                .setLargeIcon(icon)
                .setColor(mResources.getColor(R.color.colorPrimary))
                .setLights(mResources.getColor(R.color.colorPrimary), 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.mipmap.ic_launcher);

//        try {
//            String picture_url = data.get("icon");
//            if (picture_url != null && !"".equals(picture_url)) {
//                URL url = new URL(picture_url);
//                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                notificationBuilder.setStyle(
//                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
//                );
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Notification Channel is required for Android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(
                    channelID, channelName, notificationImportance
            );
            channel.setDescription(channelDescription);
            channel.setShowBadge(true);
            channel.setSound(notificationSoundUri, null);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(mResources.getColor(R.color.colorPrimary));
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(channel);
        }//if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) closes here....
        else{
            Log.w(TAG, "SDK_INT < O");
        }

        notificationManager.notify(NotificationID.getID(), notificationBuilder.build());
    }//displayNotification closes here....

}//DisplayLocalNotification closes here....
