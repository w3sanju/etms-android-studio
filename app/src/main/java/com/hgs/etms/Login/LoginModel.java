package com.hgs.etms.Login;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginModel implements Parcelable {


    public Boolean status;
    public String message;
    public LoginDataModel data;
    public Integer totalRecords;



    protected LoginModel(Parcel in) {
        byte statusVal = in.readByte();
        status = statusVal == 0x02 ? null : statusVal != 0x00;
        message = in.readString();
        data = (LoginDataModel) in.readValue(LoginDataModel.class.getClassLoader());
        totalRecords = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (status == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (status ? 0x01 : 0x00));
        }
        dest.writeString(message);
        dest.writeValue(data);
        if (totalRecords == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(totalRecords);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LoginModel> CREATOR = new Parcelable.Creator<LoginModel>() {
        @Override
        public LoginModel createFromParcel(Parcel in) {
            return new LoginModel(in);
        }

        @Override
        public LoginModel[] newArray(int size) {
            return new LoginModel[size];
        }
    };

}//LoginModel closes here....