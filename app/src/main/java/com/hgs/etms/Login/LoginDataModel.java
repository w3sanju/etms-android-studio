package com.hgs.etms.Login;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LoginDataModel implements Parcelable {

    @NonNull public String firstName;
    @Nullable public String lastName;
    @Nullable public String email;
    @Nullable public String mobile;
    @Nullable public String apiToken;
    @Nullable public Boolean isDriverLogin;


    protected LoginDataModel(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        mobile = in.readString();
        apiToken = in.readString();
        isDriverLogin = in.readBoolean();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(apiToken);
        dest.writeBoolean(isDriverLogin);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LoginDataModel> CREATOR = new Parcelable.Creator<LoginDataModel>() {
        @Override
        public LoginDataModel createFromParcel(Parcel in) {
            return new LoginDataModel(in);
        }

        @Override
        public LoginDataModel[] newArray(int size) {
            return new LoginDataModel[size];
        }
    };

}//LoginDataModel closes here....
