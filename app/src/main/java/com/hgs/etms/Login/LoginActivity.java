package com.hgs.etms.Login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.Dashboard.Driver.DashboardActivity;
import com.hgs.etms.Dashboard.Employee.EmployeeDashboardBaseActivity;
import com.hgs.etms.R;
import com.hgs.etms.utils.AESCrypt;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

public class LoginActivity extends AppCompatActivity {

    //High priority UI variables goes below.....
    private EditText mPasswordEdtTxt;
    private EditText mUsernameEdtTxt;
    private Button mEmailSignInButton;


    //Medium priority NON-Ui variables goes below....
    private String encryptedUsername, encryptedPassword;



    //Least priority variables goes below......
    private final String TAG = "LoginActivity";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailSignInButton = findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mPasswordEdtTxt = findViewById(R.id.uPass);
        mUsernameEdtTxt = findViewById(R.id.uId);

    }//onCreate closes here....



    private void attemptLogin() {



        if (!mEmailSignInButton.isEnabled()) {
            return;
        }


        // Reset errors.
        mUsernameEdtTxt.setError(null);
        mPasswordEdtTxt.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsernameEdtTxt.getText().toString();
        String password = mPasswordEdtTxt.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(password)) {
            mPasswordEdtTxt.setText("");
            Toast.makeText(getApplicationContext(),"Empty password", Toast.LENGTH_SHORT).show();
            mPasswordEdtTxt.setError("Password too short, enter minimum 4 characters");
            focusView = mPasswordEdtTxt;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
//        else if(!TextUtils.isEmpty(password) && !isPasswordValid(password))
//        {
//            mPasswordEdtTxt.setText("");
//            Toast.makeText(getApplicationContext(),"Invalid password", Toast.LENGTH_SHORT).show();
//            mPasswordEdtTxt.setError("Invalid password");
//            focusView = mPasswordEdtTxt;
//            cancel = true;
//        }

        //TODO commented for making no restrictions on password if there are resterictions uncomment it


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {

        mUsernameEdtTxt.setText("");
            mUsernameEdtTxt.setFocusable(true);
            Toast.makeText(getApplicationContext(),"Empty user ID", Toast.LENGTH_SHORT).show();
            mUsernameEdtTxt.setError("Empty user ID");
            focusView = mUsernameEdtTxt;
            cancel = true;
        }
//        else if (!isEmailValid(email)) {
//            Toast.makeText(getApplicationContext(),"Not valid user ID", Toast.LENGTH_SHORT).show();
//            mUsernameEdtTxt.setError("Not valid user ID");
//            focusView = mUsernameEdtTxt;
//            cancel = true;
//        }


        //TODO commented for making no restrictions on login userId if there are resterictions uncomment it


        if (cancel) {
            focusView.requestFocus();
        } else {
            loginUser();
        }

    }

    private boolean isEmailValid(String email) {
//        return email.contains("@");
//        return email.contains("abcd");
        return email.length() > 4;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }







    private void loginUser(){

        //////////........FOR DRIVER.......\\\\\\\\\\\\\
//        final String uName = "1/9VIdP8bc61J55lxAxHpA==";//Sanju
//        final String pWord = "1/9VIdP8bc61J55lxAxHpA==";//Sanju


        ////////..........FOR 2ND DRIVER........\\\\\\\\\\\\
//        final String uName = "NtcOeVy+30XYbbq8tm4n8g==";//Manujath
//        final String pWord = "NtcOeVy+30XYbbq8tm4n8g==";//Manujath



        //////////.........FOR EMPLOYEE...........\\\\\\\\\\\
        //TODO: COMMENT FOR PRODUCTION CODE....
//        encryptedUsername = "v/+W49QsOPuK6j5/JjOnrg==";//166535
//        encryptedPassword = "NH3tmgv3lKlEZp0YeGv5ow==";


        ///////////........FOR PRODUCTION..........\\\\\\\\\\\\\
        //TODO: PRODUCTION CODE....
        AESCrypt cryptedObject = new AESCrypt(getString(R.string.AES_SECRET_KEY));
        encryptedUsername = cryptedObject.encrypt(mUsernameEdtTxt.getText().toString().trim());//Encrypted Username.....
        encryptedPassword = cryptedObject.encrypt(mPasswordEdtTxt.getText().toString().trim());//Encrypted Password....


        Log.d(TAG, "Login url = "+BuildConfig.SERVER_URL + "login");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.SERVER_URL + "login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response = "+response);


                        /////////.........PARSING RESPONSE IN MODEL CLASSES........\\\\\\\\\\\\\
                        LoginModel loginResponseModel = new Gson().fromJson(response, LoginModel.class);


                        if(loginResponseModel.status){
                            //Response is success....
                            Toast.makeText(LoginActivity.this, "Welcome, "+loginResponseModel.data.firstName, Toast.LENGTH_SHORT).show();


                            //////////..........SAVING DATA IN SHAREDPREFS..........\\\\\\\\\\\\\\\\
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString(AppConstants.SF_LOGGED_IN_USER_EMP_ID, mUsernameEdtTxt.getText().toString().trim()).apply();
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME, loginResponseModel.data.firstName.toString().trim()).apply();

                            if(loginResponseModel.data.isDriverLogin != null && loginResponseModel.data.isDriverLogin){
                                //not null & is Drive login, i.e. drive is trying to login...
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString(AppConstants.SF_LOGGED_IN_USER_TYPE, LoginUserTypeEnum.DRIVER.toString().trim()).apply();


                                Intent intentDriver = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivity(intentDriver);
                            }//if(loginResponseModel.data.isDriverLogin != null && loginResponseModel.data.isDriverLogin) closes here....
                            else {
                                //else employee is trying to login.....
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString(AppConstants.SF_LOGGED_IN_USER_TYPE, LoginUserTypeEnum.EMPLOYEE.toString().trim()).apply();

                                Intent intent = new Intent(LoginActivity.this, EmployeeDashboardBaseActivity.class);
                                startActivity(intent);
                            }//else closes here....

                            mUsernameEdtTxt.setText("");
                            mPasswordEdtTxt.setText("");

                            finish();//Login screen will finish, for back press....
                        }//if(loginResponseModel.status) closes here......
                        else{
                            //Else response is failure...
                            Toast.makeText(LoginActivity.this, loginResponseModel.message, Toast.LENGTH_SHORT).show();

                            mUsernameEdtTxt.setText("");
                            mPasswordEdtTxt.setText("");
                        }//else closes here.....


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(LoginActivity.this,"ErrorListener \n " + error.toString(),Toast.LENGTH_LONG).show();
                        mUsernameEdtTxt.setText("");
                        mPasswordEdtTxt.setText("");

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            // Is thrown if there's no network connection or server is down
                            Toast.makeText(LoginActivity.this, getString(R.string.error_network_timeout),
                                    Toast.LENGTH_LONG).show();
                            // We return to the last fragment
                            if (getFragmentManager().getBackStackEntryCount() != 0) {
                                getFragmentManager().popBackStack();
                            }

                        } else {
                            // Is thrown if there's no network connection or server is down
                            Toast.makeText(LoginActivity.this, getString(R.string.error_network),
                                    Toast.LENGTH_LONG).show();
                            // We return to the last fragment
                            if (getFragmentManager().getBackStackEntryCount() != 0) {
                                getFragmentManager().popBackStack();
                            }
                        }


                    }
                }){

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                Log.d(TAG, "Login Parameters : "+headers);
                return headers;

            }

            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("EmployeeId", encryptedUsername);
                params.put("Password", encryptedPassword);
                params.put(AppConstants.DEVICE_TYPE_PARAM, AppConstants.DEVICE_TYPE_VALUE);

                String deviceToken = "";
                if (PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).contains(AppConstants.SF_DEVICE_TOKEN_FCM))
                    if (PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).getString(AppConstants.SF_DEVICE_TOKEN_FCM, "") != null)
                        deviceToken = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).getString(AppConstants.SF_DEVICE_TOKEN_FCM, "");

                params.put(AppConstants.DEVICE_TOKEN_PARAM, deviceToken);

                Log.d(TAG, "Login Parameters : "+params);
                return params;
            }

        };



        stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                AppConstants.VOLLEY_MAX_RETRIES,
                AppConstants.VOLLEY_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }



    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


}
