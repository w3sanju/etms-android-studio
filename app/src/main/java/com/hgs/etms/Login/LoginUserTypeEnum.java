package com.hgs.etms.Login;

//SINCE MULTIPLE LOGIN CAN TAKE PLACE, THEREFORE WE HAVE CREATE ENUM TO HANDLE THE SAME.
public enum LoginUserTypeEnum {

    EMPLOYEE, DRIVER

}//LoginUserTypeEnum closes here....
