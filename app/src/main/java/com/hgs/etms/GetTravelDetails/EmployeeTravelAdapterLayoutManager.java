package com.hgs.etms.GetTravelDetails;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;

public class EmployeeTravelAdapterLayoutManager extends GridLayoutManager {

    private boolean isScrollEnabled = false;

    public EmployeeTravelAdapterLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
    }


    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    @Override
    public boolean canScrollVertically() {
        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        return false;
    }

}//EmployeeTravelAdapterLayoutManager closes here....
