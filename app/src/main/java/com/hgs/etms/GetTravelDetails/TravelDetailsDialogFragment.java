package com.hgs.etms.GetTravelDetails;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hgs.etms.Login.LoginUserTypeEnum;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class TravelDetailsDialogFragment extends DialogFragment implements View.OnClickListener {


    //Medium priority NON UI variables goes below....
    private TravelDetailsModel travelDetails;
    private TextView mCabDetailsTxtV, mPickUpPtTxtV;
    private RecyclerView mEmpDetailsRecyclerV;
    private LinearLayout pickUpLayout;
    private String userType;
    private View view;
    private Button mDismissBtn, mCloseBtn;


    //Least priority variables goes below....
    private final String TAG = "TravelDetailsDialogFragment".trim().substring(0,23);




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.customalert_driver, container);

        TextView tvCheckMode = view.findViewById(R.id.checkModeTxt);
        ImageView checkImg = view.findViewById(R.id.checkModeImg);

        //////......STEP:1 >> REMOVE WHITE WINDOW & MAKE TRANSPARENT........
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }


        //////.......STEP:2 >> FETCHING ARGUMENTS.......\\\\\\\\\\\\\
        if(getArguments() != null){
            travelDetails = getArguments().getParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY);
            TRAVEL_TYPE_ENUM travelType = (TRAVEL_TYPE_ENUM) getArguments().getSerializable(AppConstants.TRAVEL_TYPE_KEY);
             userType = getArguments().getString(AppConstants.SF_LOGGED_IN_USER_TYPE);

           if(travelType.name().equals("DROP")){
               tvCheckMode.setText(getActivity().getString(R.string.check_out_cab));
               checkImg.setImageResource(R.drawable.ic_car_logout);
           }else {
               tvCheckMode.setText(getActivity().getString(R.string.check_in_cab));
               checkImg.setImageResource(R.drawable.ic_car_login);
           }

        }//if(getArguments() != null) closes here.....
        else{
            Log.w(TAG, "Travel details is null, from Arguemnts");
        }//else closes here.....




        //////////........Step:3 >> Initalizing UI Views..........\\\\\\\\\\\\\\
        initializations();


        if(userType.equals(LoginUserTypeEnum.DRIVER.toString().trim())){
            pickUpLayout.setVisibility(View.GONE);



        }else {
            pickUpLayout.setVisibility(View.VISIBLE);
        }

        //////////..........Step:4 >> Setting data on the UI............\\\\\\\\\\\\
        String cabNo = "Vehicle number : "+travelDetails.data.get(0).vehicle_reg_no;
        String cabColorDetails = "Vehicle Details : N/A";
        mCabDetailsTxtV.setText(cabNo+"\n"+cabColorDetails);


        String pickupPoint = travelDetails.data.get(0).home_location;
        mPickUpPtTxtV.setText(pickupPoint);


        EmployeeTravelDetailsAdapter travelDetailsAdapter = new EmployeeTravelDetailsAdapter(travelDetails,getActivity(),userType);
        mEmpDetailsRecyclerV.setLayoutManager(new EmployeeTravelAdapterLayoutManager(getActivity(), 2));
        mEmpDetailsRecyclerV.setAdapter(travelDetailsAdapter);


        mDismissBtn = view.findViewById(R.id.btnDismiss);
        mCloseBtn = view.findViewById(R.id.btnClose);
        mDismissBtn.setOnClickListener(this);

        mCloseBtn.setOnClickListener(this);

        return view;
    }//onCreateView closes here....


    private void initializations() {
        mCabDetailsTxtV = view.findViewById(R.id.txtVcabDetails);
        mPickUpPtTxtV = view.findViewById(R.id.tvPickupPt);
        mEmpDetailsRecyclerV = view.findViewById(R.id.empDetailsRecyclerV);
        pickUpLayout = view.findViewById(R.id.pickUpLayout);
    }//initializations closes here....





    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnDismiss:
                if(getDialog() != null && getDialog().isShowing())
                    getDialog().dismiss();
                else
                    Log.w(TAG, "Cannot dismiss dialog, since is null or not showing.");
                break;

            case R.id.btnClose:
                if(getDialog() != null && getDialog().isShowing())
                    getDialog().dismiss();
                else
                    Log.w(TAG, "Cannot dismiss dialog, since is null or not showing.");
                break;

                default:
                    Log.w(TAG, "Unhandled onCLick event: "+view);
                    break;
        }//switch (view.getId()) closes here....
    }//onClick closes here....
}//TravelDetailsDialogFragment closes here.....
