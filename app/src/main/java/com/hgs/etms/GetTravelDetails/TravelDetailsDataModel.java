package com.hgs.etms.GetTravelDetails;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class TravelDetailsDataModel implements Parcelable {

    public String empid;
    public String scheduled_date;
    public String gender;
    public String slot;
    public String slot_type;
    public String first_Name;
    public String last_Name;
    public String employee_Ph_no;
    public String home_location;
    public String created_date;
    @Nullable
    public String created_by;
    @Nullable
    public String delete_flag;
    public int vehicle_id;
    public String vehicle_reg_no;
    public String driver_name;
    public String driver_No;
    public Boolean checkInStatus;
    public Boolean checkOutStatus;
    @Nullable
    public String checkInDate;
    @Nullable
    public String checkOutDate;

    protected TravelDetailsDataModel(Parcel in) {
        empid = in.readString();
        scheduled_date = in.readString();
        gender = in.readString();
        slot = in.readString();
        slot_type = in.readString();
        first_Name = in.readString();
        last_Name = in.readString();
        employee_Ph_no = in.readString();
        home_location = in.readString();
        created_date = in.readString();
        created_by = in.readString();
        delete_flag = in.readString();
        vehicle_id = in.readInt();
        vehicle_reg_no = in.readString();
        driver_name = in.readString();
        driver_No = in.readString();
        byte checkInStatusVal = in.readByte();
        checkInStatus = checkInStatusVal == 0x02 ? null : checkInStatusVal != 0x00;
        byte checkOutStatusVal = in.readByte();
        checkOutStatus = checkOutStatusVal == 0x02 ? null : checkOutStatusVal != 0x00;
        checkInDate = in.readString();
        checkOutDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(empid);
        dest.writeString(scheduled_date);
        dest.writeString(gender);
        dest.writeString(slot);
        dest.writeString(slot_type);
        dest.writeString(first_Name);
        dest.writeString(last_Name);
        dest.writeString(employee_Ph_no);
        dest.writeString(home_location);
        dest.writeString(created_date);
        dest.writeString(created_by);
        dest.writeString(delete_flag);
        dest.writeInt(vehicle_id);
        dest.writeString(vehicle_reg_no);
        dest.writeString(driver_name);
        dest.writeString(driver_No);
        if (checkInStatus == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (checkInStatus ? 0x01 : 0x00));
        }
        if (checkOutStatus == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (checkOutStatus ? 0x01 : 0x00));
        }
        dest.writeString(checkInDate);
        dest.writeString(checkOutDate);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TravelDetailsDataModel> CREATOR = new Parcelable.Creator<TravelDetailsDataModel>() {
        @Override
        public TravelDetailsDataModel createFromParcel(Parcel in) {
            return new TravelDetailsDataModel(in);
        }

        @Override
        public TravelDetailsDataModel[] newArray(int size) {
            return new TravelDetailsDataModel[size];
        }
    };

}//TravelDetailsDataModel closes here....
