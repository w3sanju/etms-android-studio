package com.hgs.etms.GetTravelDetails;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hgs.etms.Login.LoginUserTypeEnum;
import com.hgs.etms.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class EmployeeTravelDetailsAdapter extends RecyclerView.Adapter {

    //High priority variables goes below....
    private final TravelDetailsModel mTravelDetailsModel;
    Context mContext;
    private String userType;


    //Least priority variables goes below....
    private final String TAG = "EmployeeTravelDetailsAdapter".trim().substring(0,23);

    //Constructor goes below....
    public EmployeeTravelDetailsAdapter(TravelDetailsModel travelDetails, Context context,String userType) {
        mTravelDetailsModel = travelDetails;
        mContext = context;
        this.userType = userType;
    }//constructor closes here.....



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_travel_details_single_row, parent, false);
        return new EmployeeTravelDetailsViewHolder(view);
    }//onCreateViewHolder closes here.....

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        EmployeeTravelDetailsViewHolder viewHolder = (EmployeeTravelDetailsViewHolder) holder;
        String employeeFName = "";
        if(mTravelDetailsModel.data.get(position).first_Name != null)
            employeeFName = mTravelDetailsModel.data.get(position).first_Name.trim();


        String travellerHtmlText = getTravellerNameText(position+1+") ", employeeFName);
        viewHolder.mEmployeeNameTxtV.setText(Html.fromHtml(travellerHtmlText));
        viewHolder.mEmployeeNameTxtV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(userType.equals(LoginUserTypeEnum.DRIVER.toString().trim())) {
                    if (mTravelDetailsModel.data.get(position).first_Name != null && mTravelDetailsModel.data.get(position).home_location.trim() != null)
                        showEmployeeAddress(mTravelDetailsModel.data.get(position).first_Name.trim(), mTravelDetailsModel.data.get(position).home_location.trim());
                    else
                        Log.w(TAG, mContext.getResources().getString(R.string.no_pick_up_available));
                }
            }
        });
        if(mTravelDetailsModel.data.get(position).checkInStatus){
            viewHolder.mEmpBoardingStatusImgV.setVisibility(View.VISIBLE);
            viewHolder.mEmpBoardingStatusImgV.setImageResource(R.drawable.ic_done);
        }//if(mTravelDetailsModel.data.get(position).checkInStatus) closes here....
        else{
            viewHolder.mEmpBoardingStatusImgV.setVisibility(View.GONE);
        }//else closes here....

    }//onBindViewHolder closes here.....

    @Override
    public int getItemCount() {
        if(mTravelDetailsModel == null)
            return 0;
        else
            return mTravelDetailsModel.data.size();
    }//getItemCount closes here....


    /**
     * This function returns text with different color like Spannable etc.
     * **/
    private String getTravellerNameText(String color1Text, String color2Text){
        String text = "<font color=#000000>"+color1Text+"</font> <font color=#1f90d8>"+color2Text+"</font>";
        return text;
    }//getTravellerNameText closes here....



    private class EmployeeTravelDetailsViewHolder extends RecyclerView.ViewHolder {

        private TextView mEmployeeNameTxtV;
        private ImageView mEmpBoardingStatusImgV;

        public EmployeeTravelDetailsViewHolder(View view) {
            super(view);

            mEmployeeNameTxtV = view.findViewById(R.id.employeeNameTxtV);
            mEmpBoardingStatusImgV = view.findViewById(R.id.empBoardingFlagImgV);
        }
    }//EmployeeTravelDetailsViewHolder closes here.....

   private void showEmployeeAddress(String name,String address){



       AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(mContext.getString(R.string.employe_info));
        alertDialog.setMessage(mContext.getString(R.string.employee_name)+" "+ name+ "\n\n" +mContext.getString(R.string.employee_address)+" " +address);
        alertDialog.setPositiveButton(mContext.getString(R.string.OK), null).show();

    }

}//EmployeeTravelDetailsAdapter class closes here.....
