package com.hgs.etms.GetTravelDetails;

public interface GetTravelDetailsListener {

    /**
     * @travelType: This will tell whether user is travelling for Drop or Pickup
     * **/
    void travelDetailsSuccess(TravelDetailsModel travelDetailsModel, TRAVEL_TYPE_ENUM travelType);

    void travelDetailsFailure(String errorMessage);

}//GetTravelDetailsListener closes here.....
