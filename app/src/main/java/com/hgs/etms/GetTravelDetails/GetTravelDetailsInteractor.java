package com.hgs.etms.GetTravelDetails;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hgs.etms.BuildConfig;
import com.hgs.etms.Dashboard.Driver.PICKUP_DROP_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;

public class GetTravelDetailsInteractor {

    //High priority UI variables goes below.....
    private GetTravelDetailsListener travelResponseListener;
    private TRAVEL_TYPE_ENUM travelType;
    private Context mContext;


    //Least priority variables goes below.....
    private final String TAG = "GetTravelDetailsInteractor".toString().trim().substring(0,23);
    private final String getTravelDetailsUrl = "ScheduleStatus";



    public GetTravelDetailsInteractor(GetTravelDetailsListener travelResponseListener, Context context, TRAVEL_TYPE_ENUM travelType) {
        this.travelResponseListener = travelResponseListener;
        this.mContext = context;
        this.travelType = travelType;
    }//GetTravelDetailsInteractor constructor closes here.....



    public void getTravelDetails(@NonNull String scheduledDate, @NonNull String scheduledSlot, @NonNull String driverName){

        final Map<String, String> params = new HashMap<String, String>();
        params.put(AppConstants.SLOT_KEY, scheduledSlot);
        params.put(AppConstants.SCHEDULED_DATE_KEY, scheduledDate);
        params.put(AppConstants.DRIVER_NAME_KEY, driverName);

        String slotType = ""+ PICKUP_DROP_TYPE_ENUM.LOGIN;
        switch (travelType){
            case PICKUP:
                slotType = ""+PICKUP_DROP_TYPE_ENUM.LOGIN;
                break;
            case DROP:
                slotType = ""+PICKUP_DROP_TYPE_ENUM.LOGOUT;
                break;
        }//switch (mTravelType) closes here.....
        params.put(AppConstants.SLOT_TYPE_KEY, slotType);



        if(this.travelResponseListener == null)
            throw new NullPointerException("travelResponseListener cannot be null.");
        else {
            String apiUrl = BuildConfig.SERVER_URL + getTravelDetailsUrl;
            Log.d(TAG, "URL = " + apiUrl);
            Log.d(TAG, "Params = " + params);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, apiUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "Response is : " + response);


                            TravelDetailsModel travelDetailsModel = new Gson().fromJson(response, TravelDetailsModel.class);

                            ////////////...........PARSING RESPONSE..........\\\\\\\\\\\\\\
                            if (travelDetailsModel.status) {
                                //Status is true.

                                /**
                                 * The API is giving all the Login & Logout entries common, so we have to filter the array manually & then send the data to the View.
                                 * **/
//                                Log.d(TAG, "Original list size = "+travelDetailsModel.data.size());
                                List<TravelDetailsDataModel> filteredTravelDetailsDataList = getFilteredList(travelDetailsModel.data);
                                travelDetailsModel.data = filteredTravelDetailsDataList;//Saving the filtered list in then object....
//                                Log.d(TAG, "Filtered list size = "+travelDetailsModel.data.size());


                                travelResponseListener.travelDetailsSuccess(travelDetailsModel, travelType);
                            }//if(travelDetailsModel.status) closes here....
                            else {
                                //Status is false.
                                travelResponseListener.travelDetailsFailure(travelDetailsModel.message);
                            }//else closes here....
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getActivity(), "ErrorListener \n " + error.toString(), Toast.LENGTH_LONG).show();
                            travelResponseListener.travelDetailsFailure(error.getMessage()+" "+error.toString());
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");//TODO: Token is Hardcoded, need to softcode it.
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    return params;
                }

            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(AppConstants.VOLLEY_TIMEOUT,
                    AppConstants.VOLLEY_MAX_RETRIES,
                    AppConstants.VOLLEY_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this.mContext);
            requestQueue.add(stringRequest);
        }//else listener is not null closes here......
    }//getTravelDetails closes here.....



    /**
     * This fucntion will filter the list w.r.t the LOGIN or LOGOUT.
     *
     * @param originalListToFilter : The list on which you want to  Filter.
     * **/
    private List<TravelDetailsDataModel> getFilteredList(final List<TravelDetailsDataModel> originalListToFilter) {
        final List<TravelDetailsDataModel> filteredTravelDetailsDataList = new ArrayList<TravelDetailsDataModel>();

        for (TravelDetailsDataModel dataModel : originalListToFilter) {
            PICKUP_DROP_TYPE_ENUM slotType = PICKUP_DROP_TYPE_ENUM.valueOf(dataModel.slot_type);

            switch (travelType) {
                case PICKUP:
                    //Since this request is for Pickup, so we want to filter the list with LOGIN slotType....
                    if (slotType == PICKUP_DROP_TYPE_ENUM.LOGIN)
                        filteredTravelDetailsDataList.add(dataModel);
                    break;

                case DROP:
                    //Since the request is for Drop, so we want to filter the list with LOGOUT slotType....
                    if (slotType == PICKUP_DROP_TYPE_ENUM.LOGOUT)
                        filteredTravelDetailsDataList.add(dataModel);
                    break;
            }//switch (mTravelType) closes here.....
        }//for(TravelDetailsDataModel dataModel :travelDetailsModel.data) closes here....

        return filteredTravelDetailsDataList;
    }//getFilteredList closes here....

}//GetTravelDetailsInteractor closes here....
