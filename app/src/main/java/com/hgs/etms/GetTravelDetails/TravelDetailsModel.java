package com.hgs.etms.GetTravelDetails;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class TravelDetailsModel implements Parcelable {

    public boolean status;
    public String message;
    @Nullable public List<TravelDetailsDataModel> data;
    private int totalRecords;


    protected TravelDetailsModel(Parcel in) {
        status = in.readByte() != 0x00;
        message = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<TravelDetailsDataModel>();
            in.readList(data, TravelDetailsDataModel.class.getClassLoader());
        } else {
            data = null;
        }
        totalRecords = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (status ? 0x01 : 0x00));
        dest.writeString(message);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        dest.writeInt(totalRecords);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TravelDetailsModel> CREATOR = new Parcelable.Creator<TravelDetailsModel>() {
        @Override
        public TravelDetailsModel createFromParcel(Parcel in) {
            return new TravelDetailsModel(in);
        }

        @Override
        public TravelDetailsModel[] newArray(int size) {
            return new TravelDetailsModel[size];
        }
    };

}//TravelDetailsModel closes here....
