package com.hgs.etms.Logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.hgs.etms.Login.LoginActivity;
import com.hgs.etms.utils.AppConstants;

import androidx.preference.PreferenceManager;

public class Logout {

    private final Context mContext;

    public Logout(Context mContext) {
        this.mContext = mContext;
    }//constructor closes here.....


    public void execute(){

        if(mContext == null)
            throw new NullPointerException("mContext cannot be null.");

        //01. We will clear the maximum values in SharedPrefs except the DEVICE TOKEN....
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor sharedPrefsEditor = sharedPrefs.edit();
        sharedPrefsEditor.remove(AppConstants.SF_LOGGED_IN_USER_TYPE);
        sharedPrefsEditor.remove(AppConstants.SF_LOGGED_IN_USER_EMP_ID);
        sharedPrefsEditor.remove(AppConstants.SF_STARTED_SLOTS_AND_DATE);
        sharedPrefsEditor.remove(AppConstants.SF_LOGGED_IN_USER_EMP_FNAME);
        sharedPrefsEditor.apply();
        //DO NOT REMOVE THE DEVICE TOKEN FROM SHARED PREFERENCE.


        //02. Intent to Login now....
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
    }//execute closes here.....



}//Logout closes here....
