package com.hgs.etms;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckInHandler.CheckInInteractor;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckInHandler.CheckInResponseListener;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckInHandler.CheckInResponseModel;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutInteractor;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutResponseListener;
import com.hgs.etms.Dashboard.Employee.EmployeeCheckOutHandler.CheckOutResponseModel;
import com.hgs.etms.Dashboard.Employee.EmployeeDashboardBaseActivity;
import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.GetTravelDetails.TRAVEL_TYPE_ENUM;
import com.hgs.etms.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;


public class ScanActivity extends AppCompatActivity implements View.OnClickListener, CheckOutResponseListener, CheckInResponseListener {

    public TextView ResultLabel;
    private String latitude, longitude;
    private int scanType, positionInAdapter;
    private GetAllTripsDataModel mTripDetailsObject;


    private final String TAG = "ScanActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        ResultLabel = findViewById(R.id.ResultLabel);

        //Fetching Data.....
        if (getIntent().getExtras() != null) {
            mTripDetailsObject = getIntent().getParcelableExtra(AppConstants.TRAVEL_DETAILS_OBJECT_KEY);
            latitude = getIntent().getStringExtra(AppConstants.EMPLOYEE_LATITUDE);
            longitude = getIntent().getStringExtra(AppConstants.EMPLOYEE_LONGITUDE);
            scanType = getIntent().getIntExtra("scanType", -1);
            positionInAdapter = getIntent().getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, -1);
        }//if(getIntent().getExtras() != null) closes here....
        else
            Log.w(TAG, "getIntent().getExtras is null.");


        //Scan Button
        final Button buttonScan = (Button) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(this);
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setPrompt("Scan QR code");
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.setTimeout(50000);
        integrator.initiateScan();
    }


    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {

                Intent intent = new Intent(this, EmployeeDashboardBaseActivity.class);
                startActivity(intent);
                finish();
                Toast.makeText(this, "Scan Cancelled", Toast.LENGTH_LONG).show();

            } else {

                try {
                    JSONObject jsonObjectY = new JSONObject(result.getContents());

                    final String qrCode_VehicleID = jsonObjectY.getString("VehicleId");
                    Map<String, String> startTripParams = new HashMap<>();
                    startTripParams.put(AppConstants.SCHEDULED_DATE_KEY, mTripDetailsObject.scheduled_date);
                    startTripParams.put(AppConstants.VEHICLE_ID, qrCode_VehicleID);
                    startTripParams.put(AppConstants.SLOT_KEY, mTripDetailsObject.slot);
                    startTripParams.put(AppConstants.EMPLOYEE_ID, mTripDetailsObject.empid);


                    switch (scanType) {
                        case 0:
                            //Checkin API should be called here....
                            startTripParams.put(AppConstants.CHECK_IN_LATITUDE_KEY, String.valueOf(latitude));
                            startTripParams.put(AppConstants.CHECK_IN_LONGITUDE_KEY,String.valueOf(longitude));
                            CheckInInteractor checkInInteractor = new CheckInInteractor(this, null, ScanActivity.this);
                            checkInInteractor.execute(startTripParams, positionInAdapter);
                            break;

                        case 1:
                            //Checkout API should be called here....
                            startTripParams.put(AppConstants.CHECK_OUT_LATITUDE_KEY, String.valueOf(latitude));
                            startTripParams.put(AppConstants.CHECK_OUT_LONGITUDE_KEY,String.valueOf(longitude));
                            CheckOutInteractor checkOutInteractor = new CheckOutInteractor(this, null, ScanActivity.this);
                            checkOutInteractor.execute(startTripParams, positionInAdapter);
                            break;

                        default:
                            Log.w(TAG, "Unhandled scanType = " + scanType);
                            break;
                    }//switch (scanType) closes here.....

//                    scannedData(vehicle);

                } catch (JSONException e) {

                    Intent intent = new Intent(this, EmployeeDashboardBaseActivity.class);
                    startActivity(intent);
                    finish();

                    Toast.makeText(this, "Invalid QR !", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        } else {
            Intent intent = new Intent(this, EmployeeDashboardBaseActivity.class);
            startActivity(intent);
            finish();

            Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onClick(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setPrompt("Scan QR code");
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.setTimeout(50000);
        integrator.initiateScan();
    }

    @Override
    public void employeeCheckOutSuccessFully(CheckOutResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        Toast.makeText(this, responseModel.message, Toast.LENGTH_SHORT).show();

        Intent broadcastScanResult = new Intent(AppConstants.EMPLOYEE_CHECKOUT_BROADCAST);
        broadcastScanResult.putExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, getIntent().getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, 0));
        this.sendBroadcast(broadcastScanResult);


        finish();
        finish();


    }//employeeCheckOutSuccessFully closes here.....

    @Override
    public void employeeCheckOutFailure(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        finish();
        finish();
    }//employeeCheckOutFailure closes here.....

    @Override
    public void employeeCheckInSuccessFully(CheckInResponseModel responseModel, TRAVEL_TYPE_ENUM travelType, Integer pos) {
        Toast.makeText(this, responseModel.message, Toast.LENGTH_SHORT).show();

        Intent broadcastScanResult = new Intent(AppConstants.EMPLOYEE_CHECK_IN_BROADCAST);
        broadcastScanResult.putExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, getIntent().getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, 0));
        this.sendBroadcast(broadcastScanResult);

        finish();
        finish();
    }//employeeCheckInSuccessFully closes here....

    @Override
    public void employeeCheckInFailure(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();

        finish();
        finish();
    }//employeeCheckInFailure closes here.....


//    private void scannedData(String vehicle) {
//
//        SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd-MM-yyyy");
//        Date GetDate = new Date();
//
//
//        final String employeeID = PreferenceManager.getDefaultSharedPreferences(this).getString(AppConstants.SF_LOGGED_IN_USER_EMP_ID, "");
//        final String vehicleId = vehicle;
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.SERVER_URL + scanAPIUrl,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d(TAG, "Response to checkin Employee = " + response);
//                        parseQrData(response, vehicleId);
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(ScanActivity.this, "ErrorListener \n " + error.toString(), Toast.LENGTH_LONG).show();
//                    }
//                }) {
//
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/x-www-form-urlencoded");
//                headers.put("Token", "QzsGrJc210gEoUC4BznHT7Q2WsyKM5Zu");
//                return headers;
//            }
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("employeeId", employeeID);
//                params.put("ScheduledDate", scheduledDate);
//                params.put("VehicleId", vehicleId);
//                params.put("Slot", timeSlot);
//                params.put(AppConstants.CHECK_IN_LATITUDE_KEY, latitude);
//                params.put(AppConstants.CHECK_IN_LONGITUDE_KEY, longitude);
//
//                Log.d(TAG, "Params to checkin Employee = " + params);
//                return params;
//            }
//
//        };
//
//    }


//    public void parseQrData(String response, String vehicleNumber) {
//
//        try {
//
//            JSONObject jsonObjectA = new JSONObject(response);
//
//            if (jsonObjectA.getString("status").equals("true")) {
//
//
//                Intent broadcastScanResult = new Intent(AppConstants.EMPLOYEE_CHECK_IN_BROADCAST);
//                broadcastScanResult.putExtra("CheckinStatus", "CheckinDone");
//                broadcastScanResult.putExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, getIntent().getIntExtra(AppConstants.CLICKED_POSITION_INTENT_KEY, 0));
//                this.sendBroadcast(broadcastScanResult);
//
//                finish();
//                finish();
//
//
//            } else {
//                Toast.makeText(this, "Check in : Failed \nPost Data response \n status is False :", Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(this, EmployeeDashboardBaseActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

}
