package com.hgs.etms;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.hgs.etms.Dashboard.Driver.DashboardActivity;
import com.hgs.etms.Dashboard.Employee.EmployeeDashboardBaseActivity;
import com.hgs.etms.Login.LoginActivity;
import com.hgs.etms.Login.LoginUserTypeEnum;
import com.hgs.etms.utils.AppConstants;
import com.hgs.etms.utils.ETMSMessagingService;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

public class MainActivity extends AppCompatActivity {


    //Least priority variables goes below....
    private final String TAG = "MainActivity";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Step:1 >> Start the Service....
        startService(new Intent(this, ETMSMessagingService.class));

        //Step:2 >> Decide which screen to open....
        //We will check if user is already Logged in or not....
        //Since we have to delay for some time, therefore adding a delay using Handler thread....
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Write whatever to want to do after delay specified (1 sec)
                if(!PreferenceManager.getDefaultSharedPreferences(MainActivity.this).contains(AppConstants.SF_LOGGED_IN_USER_TYPE)){//Not condition....
                    //If SharedPrefs is empty, this means nobody has logged in...
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }//if(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getAll().isEmpty()) closes here....
                else{
                    //Checking if Driver is logged in, or a Employee is logged in.
                    LoginUserTypeEnum loginType = LoginUserTypeEnum.valueOf(PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString(AppConstants.SF_LOGGED_IN_USER_TYPE, ""));
                    switch (loginType){
                        case EMPLOYEE:
                            //Employee has Logged in....
                            Intent intent = new Intent(MainActivity.this, EmployeeDashboardBaseActivity.class);
                            startActivity(intent);
                            break;

                        case DRIVER:
                            //Driver has Logged in....
                            Intent intentDriver = new Intent(MainActivity.this, DashboardActivity.class);
                            startActivity(intentDriver);
                            break;
                    }//switch (loginType) closes here....
                }//else closes here....
            }//run closes here....
        }, 1000);




    }//onCreate closes here.....


}//MainActivity closes here.....
