package com.hgs.etms.Feedback;

public interface feedbackResponseListener {

    void feedbackSubmittedSuccess(FeedbackResponseModel feedbackResponseModel,  Integer pos);

    void feedbackSubmittedFailure(String errorMessage);

}//StartTripResponseListener closes here.....
