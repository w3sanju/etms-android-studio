package com.hgs.etms.Feedback;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FeedbackResponseModel implements Parcelable {


    public boolean status;
    public String message;
    @Nullable
    public List<Object> data;
    public int totalRecords;


    protected FeedbackResponseModel(Parcel in) {
        status = in.readByte() != 0x00;
        message = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<Object>();
            in.readList(data, Object.class.getClassLoader());
        } else {
            data = null;
        }
        totalRecords = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (status ? 0x01 : 0x00));
        dest.writeString(message);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
        dest.writeInt(totalRecords);
    }

    @SuppressWarnings("unused")
    public static final Creator<FeedbackResponseModel> CREATOR = new Creator<FeedbackResponseModel>() {
        @Override
        public FeedbackResponseModel createFromParcel(Parcel in) {
            return new FeedbackResponseModel(in);
        }

        @Override
        public FeedbackResponseModel[] newArray(int size) {
            return new FeedbackResponseModel[size];
        }
    };
}