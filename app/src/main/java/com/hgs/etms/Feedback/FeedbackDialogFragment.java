package com.hgs.etms.Feedback;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hgs.etms.Dashboard.Employee.GetAllTripsListing.GetAllTripsDataModel;
import com.hgs.etms.R;
import com.hgs.etms.utils.AppConstants;

import java.util.HashMap;
import java.util.Map;

public class FeedbackDialogFragment extends DialogFragment implements View.OnClickListener,feedbackResponseListener {


    //Medium priority NON UI variables goes below....
    private GetAllTripsDataModel travelDetails;
    private TextView mCabDetailsTxtV, mPickUpPtTxtV;
    private View view;
    private Button  mCloseBtn;
    private RatingBar ratingBar;
    private EditText feedbackComment;
    private Dialog dialog;


    //Least priority variables goes below....
    private final String TAG = "TravelDetailsDialogFragment".trim().substring(0,23);




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.rating_alert, container);
        setCancelable(false);


        //////......STEP:1 >> REMOVE WHITE WINDOW & MAKE TRANSPARENT........
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);


            dialog = getDialog();
        }


        //////.......STEP:2 >> FETCHING ARGUMENTS.......\\\\\\\\\\\\\
        if(getArguments() != null){
            travelDetails = getArguments().getParcelable(AppConstants.TRAVEL_DETAILS_OBJECT_KEY);
        }//if(getArguments() != null) closes here.....
        else{
            Log.w(TAG, "Travel details is null, from Arguemnts");
        }//else closes here.....




        //////////........Step:3 >> Initalizing UI Views..........\\\\\\\\\\\\\\
        initializations();


        //////////..........Step:4 >> Setting data on the UI............\\\\\\\\\\\\
        String cabNo = "Vehicle number : "+travelDetails.vehicle_id;
        String cabColorDetails = "Vehicle Details : N/A";
        mCabDetailsTxtV.setText(cabNo+"\n"+cabColorDetails);


        String pickupPoint = travelDetails.slot +" "+ "|" + " "+ travelDetails.scheduled_date;
        mPickUpPtTxtV.setText(pickupPoint);

        mCloseBtn.setOnClickListener(this);

        return view;
    }//onCreateView closes here....


    private void initializations() {
        mPickUpPtTxtV = view.findViewById(R.id.slotDetails);
        mCabDetailsTxtV = view.findViewById(R.id.cabDetails);
        ratingBar = view.findViewById(R.id.ratingBar);
        feedbackComment = view.findViewById(R.id.feedbackComment);
        mCloseBtn= view.findViewById(R.id.btnClose);


    }//initializations closes here....


    @Override
    public void onResume() {
        super.onResume();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener()
        {
            @Override
            public boolean onKey(android.content.DialogInterface dialog,
                                 int keyCode,android.view.KeyEvent event)
            {
                if ((keyCode ==  android.view.KeyEvent.KEYCODE_BACK))
                {
                    // To dismiss the fragment when the back-button is pressed.
                   // dismiss();
                    return false;
                }
                // Otherwise, do nothing else
                else return false;
            }

        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btnClose:

                submitFeedback();

                break;

                default:
                    Log.w(TAG, "Unhandled onCLick event: "+view);
                    break;
        }//switch (view.getId()) closes here....
    }//onClick closes here....

    public void submitFeedback(){

       String feedback =  feedbackComment.getText().toString().trim();
       String rating =  String.valueOf(ratingBar.getRating());
        if(rating.equals("0.0")){
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_ratings),Toast.LENGTH_LONG).show();
        }
      else  if(feedback.isEmpty()) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.enter_comment),Toast.LENGTH_LONG).show();
        } else {

           Map<String, String> feedbackParams = new HashMap<>();
           feedbackParams.put(AppConstants.VEHICLE_ID, travelDetails.vehicle_id);
           feedbackParams.put(AppConstants.SCHEDULED_DATE_KEY, travelDetails.scheduled_date);
           feedbackParams.put(AppConstants.SLOT_KEY, travelDetails.slot);
           feedbackParams.put(AppConstants.EMPLOYEE_ID, travelDetails.empid);
           feedbackParams.put(AppConstants.RATING_KEY,rating );
           feedbackParams.put(AppConstants.FEEDBACK_KEY, feedbackComment.getText().toString().trim());


           Log.d("Params", "" + feedbackParams);



           feedbackSubmitInteractor stopTripInteractor = new feedbackSubmitInteractor(this, getActivity());
           stopTripInteractor.execute(feedbackParams);
       }
    }

    @Override
    public void feedbackSubmittedSuccess(FeedbackResponseModel feedbackResponseModel, Integer pos) {

        if(feedbackResponseModel.status){
            Toast.makeText(getActivity(), R.string.thanks_for_feedback,Toast.LENGTH_LONG).show();

            if(dialog != null && dialog.isShowing())
                dialog.dismiss();
        }
    }

    @Override
    public void feedbackSubmittedFailure(String errorMessage) {
        Toast.makeText(getActivity(),errorMessage,Toast.LENGTH_LONG).show();


    }
}//TravelDetailsDialogFragment closes here.....
