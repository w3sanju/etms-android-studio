package com.hgs.etms;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.hgs.etms.utils.AppConstants;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;

public class ETMSApplication extends Application {


    //Least priority variables goes below....
    private final String TAG = "ETMSApplication";

    @Override
    public void onCreate() {
        super.onCreate();

        //Step:1 >> Get the FCM Device Token....
        saveFirebaseDeviceToken();
    }//onCreate closes here....



    private void saveFirebaseDeviceToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.d(TAG, "token = "+token);

                        // Log and toast
                        PreferenceManager.getDefaultSharedPreferences(ETMSApplication.this).edit().putString(AppConstants.SF_DEVICE_TOKEN_FCM, token).commit();
                    }
                });
    }//saveFirebaseDeviceToken closes here....
}//ETMSApplication closes here....


